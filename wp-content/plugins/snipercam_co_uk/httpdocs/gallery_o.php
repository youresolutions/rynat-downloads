<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['about_us'], $ini_array['description']['about_us'], $ini_array['keywords']['about_us'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Gallery</h1>
<p><a href="moulding.php">What is Rotational Moulding?</a> <img src="images/bullet.gif" alt="|"> <a href="moulds.php"> The Moulds &amp; Materials</a> <img src="images/bullet.gif"> <a href="design.php">Design &amp; Application</a> <img src="images/bullet.gif"> <a href="rotanewspage.php">News Page</a></p>
<ul>
<li><a href="#ga1"><strong>Gallery 1</strong></a></li>
<li><a href="#ga2"><strong>Gallery 2</strong></a></li>
<li><a href="#ga3"><strong>Gallery 3</strong></a></li>
</ul>

<div class="imgcontainer">
<div id="ga1">
<h2>Gallery 1</h2>
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
</div>
<div>
<h2 id="ga2">Gallery 2</h2>
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
</div>
<div>
<h2 id="ga3">Gallery 3</h2>
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img1large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		  <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img2large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
		 <img title="Description of what the photo is of, and where it was taken." class="img" src="images/spacer.gif" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal_img3large.jpg&amp;title=Description of what the photo is of, and where it was taken.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
</div>
</div>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>