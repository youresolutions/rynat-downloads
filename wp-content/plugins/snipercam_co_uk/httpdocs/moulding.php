<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['moulding'], $ini_array['description']['moulding'], $ini_array['keywords']['moulding'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['moulding']; ?></h1>
<p><a href="moulds.php" title="The Moulds &amp; Materials" class="gallery">The Moulds &amp; Materials</a> <img src="images/bullet.gif" alt="|" /> <a href="design.php" title="Design &amp; Application" class="gallery">Design &amp; Application</a> <img src="images/bullet.gif" alt="|" /> <a href="gallery.php" title="Product Gallery" class="gallery">Product Gallery</a> <img src="images/bullet.gif" alt="|" /> <a href="rotanewspage.php" title="News Page" class="gallery">News Page</a></p>
<h2>Rotamoulding has grown into one of the fastest moving sectors of polymer forming in Europe.</h2>

<p>A relative newcomer on the plastics processing scene, it is now beginning to realise its considerable technical and design potential, and in the last five years has acquired a high degree of sophistication.</p>

<p>These materials and technological advances, combined with well designed surface finishes and graphics, are leading the rotamoulding process away from traditional solutions into new areas of product application.</p>
<p>New materials, process control and a growing awareness of the market value of good design are combining to propel rotamoulding from the early days of doll's heads and footballs to the high specification products of the future.</p>

<p>Rotamoulding is a most cost effective alternative to blow moulding. vacuum forming, GRP moulding and fabrication, and is ideal for shaping complex customised mouldings.
Productions runs using this process can range from as law as ten to thousands.
Its suitability for low volume production extends to affordable pre-production prototyping.</p>

<p>However, it is through its design accessibility that rotamoulding offers its design accessibility that rotamoulding offers its greatest potential, revealing itself as a process previously eclipsed by other high speed techniques.</p>

<p>A housing formerly fabricated from several metal components can now be replaced by one rotamoulding incorporating information graphics, electro-mechanical devices, electronic interface features and recyclability.
</p>
<img src="images/rotational_moulding.jpg" class="flt_cntr" alt="" title="Rotational Moulding" />
<h2>The rotational moulding process, illustrated above, is explained more fully as follows:</h2>
<ul>
  <LI>A hollow moulding tool is charged with a pre&ndash;determined amount of plastic   material, usually in powder form.   
  <LI>The tool is then conveyed into an oven and rotated slowly and continuously   in its vertical and horizontal axis. The heat from the oven is transferred   through the tool wall and at the fusion temperature the plastic starts to adhere   to the surface.<BR />
    This process continues until all the material is fused.   
  <LI>The tool is then conveyed, still rotating, to a cooling chamber, where air   and water sprays are employed to cool the tool until the moulded part   solidifies.   
  <LI>The final stage involves conveying the tool to the loading/unloading station   where the tool is opened and the moulded part is removed.</LI>
   </ul>
     <p> <STRONG>The cycle   is then repeated...</STRONG></p>
	 
 
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>