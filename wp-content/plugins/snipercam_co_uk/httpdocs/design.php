<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['design'], $ini_array['description']['design'], $ini_array['keywords']['design'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['design']; ?></h1>
<p><a href="moulding.php" class="gallery" title="What is Rotational Moulding?">What is Rotational Moulding?</a> <img src="images/bullet.gif" alt="|" /> <a href="moulds.php" title="The Moulds &amp; Materials" class="gallery">The Moulds &amp; Materials</a> <img src="images/bullet.gif" alt="|" /> <a href="gallery.php" title="Product Gallery" class="gallery">Product Gallery</a> <img src="images/bullet.gif" alt="|" /> <a href="rotanewspage.php" title="News Page" class="gallery">News Page</a></p>
<p>The rotamoulding process offers more design freedom and flexibility of form than many other moulding techniques.
Despite its simplicity and economical tooling, surprisingly complex forms can be achieved that are unobtainable with other processes. The advantages are many and varied:</p>
<div class="devide">

<ul>
<li>Rotamoulding does not involve high pressures, hence the <strong>low tooling costs</strong>. When rotational moulding is used as an alternative to injection or blow moulding, tooling cost can be reduced by as much as 80 &ndash; 90%.</li>
<li>Low tooling costs enable <strong>productions runs of less then 100</strong> to be realised, and make economical <strong>prototyping</strong> possible, particularly for blow moulding. In addition, experimentation with different wall thicknesses, alternative polymers and a wide variety of surface finishes is not prohibitive.</li>
<img src="images/pallets_tx.png" alt="" title="Hygenic Plastic Pallets." />
<li>Since mouldings do not have to withstand high pressure during the rotational process, they are virtually <strong>stress free</strong>. In comparison with high pressure moulding processes they are less likely to suffer stress cracking in use.</li>
<li>It's easier to make changes to a rotational mould than to other types of mould, with the advantage that <strong>lead times can be cut to a minimum</strong>. In some cases products can go from CAD concept to production in a matter of weeks. This means that design can reflect market requirements.</li>
<li>Depending on design, <strong>Hollow shapes can be moulded without seams</strong>, sprues or ejection marks.</li>
<li><strong>Wall thickness normally remains constant</strong> throughout the entire moulding and can be even greater than 10mm. If required, it can be <strong>Increased</strong> where service stress is likely to be applied.</li>
<li>Screw threads bosses and metal inserts can easily be <strong>moulded</strong> in, reducing expensive secondary fabrication.</li>
<li><strong>Excellent load bearing properties</strong> can be achieved with twin-wall cavities or by foam-filling the spaces between the walls.</li>
<li>Rotamoulding is ideal not only for tough rigid shapes but also for <strong>flexible mouldings</strong> with deep undercuts, used in the manufacture of inflatable products such as rescue crafts and toys.</li>
<li>Engineering resins can now be rotationally formed to produce mouldings for continuous service in both <strong>high and low temperature environments</strong>.</li><br />
</ul>
</div> 
<div class="devide">
<p><img src="images/childrens_play_tx.png" title="Picture shows childrens play equipment: vending machine casing and artificial plastic rock." alt="" /></p>


<ul>
<li>Both <strong>symmetrical and asymmetrical</strong> designs can be formed as well as complex, one piece mouldings impossible with other processes.</li>
<li>Mould surfaces can be <strong>textured</strong> for safety or aesthetic reasons, or carry moulded-in company logos, information or diagrams.</li>
<li>Rotamoulding offers great <strong>flexibility of size</strong>, from small medical squeeze bulbs to bulk storage tanks of over 15,000 litres. However, it is best suited to medium to large mouldings.</li>
<li>Rotationally moulded <strong>engineering polymers</strong> can produce high strength, minimum weight mouldings at the lowest possible cost compared with other engineering polymers moulded by other processes.</li><br />
<img src="images/dashboard_tx.png" alt="Picture shows - Dash board moulding." title="Picture shows - Dash board moulding." />
<li>Rotamoulding is one of the few processes that permits <strong>undercuts</strong>, and if these are small, draft angles can be avoided. In addition, if the design does not necessitate an internal male core, no angled taper is needed.</li><li>It's easier to make changes to a rotational mould than to other types of mould, with the advantage that <strong>lead times can be cut to a minimum</strong>. In some cases products can go from CAD concept to production in a matter of weeks. This means that design can reflect market requirements.</li>
<li>No sprues or runners means <strong>low materials wastage</strong>. Excess material that is cut from a finished component can be recycled immediately in the factory. In addition, waste-free design can be built into a mould, such as a container and lid moulded together and separated afterwards.</li>
<li>Rotamoulding offers designers the benefit of <strong>recyclability</strong>. Mouldings tend to be in one thermoplastic polymer, enabling designers to plan for material recovery.</li></ul>
</div> 
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>