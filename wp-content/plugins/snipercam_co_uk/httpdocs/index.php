<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/rightcola.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['home'], $ini_array['description']['home'], $ini_array['keywords']['home'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-a">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div id="middlecol">
  <h1>Welcome to <? echo $ini_array['client_name']['full']; ?></h1>
<p><strong>Kart Protection Systems</strong> has been estalished over 20 years by a team of people with many years hands-on experience of plastic fabrication and plastic rotational moulding.</p>
<p>This background from within the rotational moulding and plastic fabrication industry has enabled Kart Protection Systems to work with customers to design and develop both new and existing products.<br />
<a href="about-us.php" class="more">More...</a>
</p>
<p><img src="images/homeimg.jpg" alt="" title="Karting Product" /></p>
<!--<h2>Product Review</h2>
<div class="imgcontainer">
<div class="homeimg"><img src="images/homeimg1.jpg" alt="" title="Product1" /></div>
<div  class="homeimg"><img src="images/homeimg2.jpg" alt="" title="Product2" /></div>
<div  class="homeimg"><img src="images/homeimg3.jpg" alt="" title="Product3" /></div>
</div>-->
</div>
<?php
rightcola($ini_array);
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>