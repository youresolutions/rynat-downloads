<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['sitemap'], $ini_array['description']['sitemap'], $ini_array['keywords']['sitemap'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['sitemap']; ?></h1>
<div class="devide">
<ul>
<li><a href="index.php">Home</a></li>
<li><a href="about-us.php">About Us</a></li>
<li><a href="gallery.php">Gallery</a></li>
<li><a href="enquiry-form.php">Enquiry Form</a></li>
<li><a href="contact-us.php">Contact Us</a></li>
<li><a href="Sitemap.php">Site Map</a></li>
</ul>

</div> 
<div class="devide">
<ul>
	<li><a href="karting.php">Karting</a>
		<ul>
			<li><a href="chainlink.php">Chainlink</a></li>
			<li><a href="wraparound.php">Wraparound</a></li>
			<li><a href="durapod.php">Durapod</a></li>
			<li><a href="kartnewspage.php">News Page</a></li>
		</ul>
	</li>
</ul>

<ul>
	<li><a href="chainlink.php">Chain link</a>
		<ul>
			<li><a href="karting.php">Karting</a></li>
			<li><a href="wraparound.php">Wraparound</a></li>
			<li><a href="durapod.php">Durapod</a></li>
			<li><a href="kartnewspage.php">News Page</a></li>
		</ul>
	</li>
</ul>
<ul>
	<li><a href="kartnewspage.php">News Page</a>
		<ul>
			<li><a href="karting.php">Karting</a></li>
			<li><a href="chainlink.php">Chainlink</a></li>
			<li><a href="durapod.php">Durapod</a></li>
			<li><a href="wraparound.php">Wraparound</a></li>
		</ul>
	</li>
</ul>
<ul>
	<li><a href="durapod.php">Durapod</a>
		<ul>
			<li><a href="karting.php">Karting</a></li>
			<li><a href="chainlink.php">Chainlink</a></li>
			<li><a href="wraparound.php">Wraparound</a></li>
			<li><a href="kartnewspage.php">News Page</a></li>
		</ul>
	</li>
</ul>


</div> 
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>