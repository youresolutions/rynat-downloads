<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/callback.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['contact_us'], $ini_array['description']['contact_us'], $ini_array['keywords']['contact_us'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Contact Us</h1>
<p><b>Please contact us in the way that is most convenient for you by selecting one of the methods below.</b></p>
<?php
// if postcode supplied, show location map
if (!empty($ini_array['address']['postcode']))
{
	$postcode = $ini_array['address']['postcode'];
	echo "<a href=\"http://www.bing.com/maps/?lvl=15&amp;where1=S20%203FJ,%20UK&amp;FORM=MMREDR\"><img id=\"map\" src=\"images/map.gif\" alt=\"Map showing how to find us\" title=\"map\" /></a>\n";
}
// If first line of address supplied, show address
if (!empty($ini_array['address']['line_1']))
{
	$address = "";
	if (!empty($ini_array['contact_person']['client_name']))
	{
		$address .= $ini_array['contact_person']['client_name']."<br />\n";
		if (!empty($ini_array['contact_person']['job_title']))
		{
			$address .= $ini_array['contact_person']['job_title']."<br />\n";
		}
	}
	$address .= $ini_array['client_name']['full']."<br />\n";
	if (!empty($ini_array['address']['line_1'])) $address .= $ini_array['address']['line_1']."<br />\n";
	if (!empty($ini_array['address']['line_2'])) $address .= $ini_array['address']['line_2']."<br />\n";
	if (!empty($ini_array['address']['line_3'])) $address .= $ini_array['address']['line_3']."<br />\n";
	if (!empty($ini_array['address']['line_4'])) $address .= $ini_array['address']['line_4']."<br />\n";
	if (!empty($ini_array['address']['line_5'])) $address .= $ini_array['address']['line_5']."<br />\n";

	if (!empty($ini_array['address']['postcode'])) $address .= $ini_array['address']['postcode'];
	$address = preg_replace('/<br \/>\n*$/', '', $address);
	echo "<h2>By Post</h2>\n";
	echo "<p>".$address."</p>\n";
}
// Phone numbers & email addresses
$by_telephone = false;
$by_mobile = false;
$by_fax = false;
$phone_numbers = "";
if (!empty($ini_array['phone_number']['telephone_1']))
{
	if (!empty($ini_array['phone_number']['telephone_2']))
	{
		$phone_numbers .= "<b>Tel 1:</b> ".$ini_array['phone_number']['telephone_1']."<br />\n";
		$phone_numbers .= "<b>Tel 2:</b> ".$ini_array['phone_number']['telephone_2']."<br />\n";
	}
	else
	{
		$phone_numbers .= "<b>Tel:</b> ".$ini_array['phone_number']['telephone_1']."<br />\n";
	}
	$by_telephone = true;
}
if (!empty($ini_array['phone_number']['mobile_1']))
{
	if (!empty($ini_array['phone_number']['mobile_2']))
	{
		$phone_numbers .= "<b>Mob 1:</b> ".$ini_array['phone_number']['mobile_1']."<br />\n";
		$phone_numbers .= "<b>Mob 2:</b> ".$ini_array['phone_number']['mobile_2']."<br />\n";
	}
	else
	{
		$phone_numbers .= "<b>Mob:</b> ".$ini_array['phone_number']['mobile_1']."<br />\n";
	}
	$by_mobile = true;
}
if (!empty($ini_array['phone_number']['fax_1']))
{
	if (!empty($ini_array['phone_number']['fax_2']))
	{
		$phone_numbers .= "<b>Fax 1:</b> ".$ini_array['phone_number']['fax_1']."<br />\n";
		$phone_numbers .= "<b>Fax 2:</b> ".$ini_array['phone_number']['fax_2']."<br />\n";
	}
	else
	{
		$phone_numbers .= "<b>Fax:</b> ".$ini_array['phone_number']['fax_1']."<br />\n";
	}
	$by_fax = true;
}
$email_addresses = "";
if (!empty($ini_array['email']['other']))
{
	$email_addresses .= "<b>Email 1:</b> ".$ini_array['email']['primary']."<br />\n";
	$email_addresses .= "<b>Email 2:</b> ".$ini_array['email']['other'];
}
else
{
	$email_addresses .= "<b>Email:</b> <a href='mailto:".$ini_array['email']['primary']."' class='abc'>".$ini_array['email']['primary']."</a>";
}
// Form heading
if ($by_telephone && $by_mobile && $by_fax)
{
	$heading = "<h2>By Telephone, Mobile, Fax or Email</h2>\n";
}
else if ($by_telephone && $by_mobile && !$by_fax)
{
	$heading = "<h2>By Telephone, Mobile or Email</h2>\n";
}
else if ($by_telephone && !$by_mobile && $by_fax)
{
	$heading = "<h2>By Telephone, Fax or Email</h2>\n";
}
else if ($by_telephone && !$by_mobile && !$by_fax)
{
	$heading = "<h2>By Telephone or Email</h2>\n";
}
else if (!$by_telephone && $by_mobile && $by_fax)
{
	$heading = "<h2>By Mobile, Fax or Email</h2>\n";
}
else if (!$by_telephone && $by_mobile && !$by_fax)
{
	$heading = "<h2>By Mobile or Email</h2>\n";
}
else if (!$by_telephone && !$by_mobile && $by_fax)
{
	$heading = "<h2>By Fax or Email</h2>\n";
}
else
{
	$heading = "<h2>By Email</h2>\n";
}
echo $heading;
echo "<p>".$phone_numbers.$email_addresses."</p>\n";
// if postcode supplied, show Multimap link to location map
if (!empty($ini_array['address']['postcode']))

echo "<h2>Request Callback</h2>\n";
callback($ini_array['client_name']['short'], $ini_array['email']['primary']);
?>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>