<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['moulds'], $ini_array['description']['moulds'], $ini_array['keywords']['moulds'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['moulds']; ?></h1>
<p><a href="moulding.php" title="What is Rotational Moulding?" class="gallery">What is Rotational Moulding?</a> <img src="images/bullet.gif" alt="|" /> <a href="design.php" title="Design &amp; Application" class="gallery">Design &amp; Application</a> <img src="images/bullet.gif" alt="|" /> <a href="gallery.php" title="Product Gallery" class="gallery">Product Gallery</a> <img src="images/bullet.gif" alt="|" /> <a href="rotanewspage.php" title="News Page" class="gallery">News Page</a></p>
<div class="devide">
<h2>The Moulds</h2>

<p>A major advantage of rotamoulding is the low tooling cost. Since the heating and fusing of resin inside a slowly rotating closed mould does not require high pressure, moulds therefore need not be as costly or complex as those tooled for other processes. Depending on requirements, moulds can be made from several materials:</p>
<p><img src="images/watertank.png" alt="" title="Picture shows water tank c/w mounting points." /></p>
<p><strong>Cast aluminium alloy:</strong> the popular choice, with good heat transfer and excellent mould definition. It is best for small to medium sized mouldings.</p>

<p><strong>Sheet mild steel:</strong> for fabricating moulds for large mouldings.</p>

<p><strong>Cast beryllium copper:</strong> for quality moulds producing very detailed surface finishes.</p>

<p><strong>Electroformed copper nickel:</strong> suitable for undercuts and the highest surface reproduction and finish.

</p>
<p>
<img src="images/wheelchair.png" alt="" title="Picture shows complete set of electric wheel chair mouldings." />
</p>

</div> 
<div class="devide">
<h2>Materials</h2>

<p>90% of rotational mouldings are made of polyethylene (PE), a polymer with immense versatility ranging from food grade to vandal-resistant and flame retardant grades for handling waste materials, fuels and corrosive substances.</p>

<p>Cross-linkable PE has been developed with superior resistance to chemicals and environmental stress cracking suitable for the production of canoes, chemical plant and high performance containers.</p>

<p>In addition to polyvinyl chloride, EVA, polyester elastomers and certain rubbers for moulding flexible and inflatable products, engineering polymers have also been developed including polypropylene, nylon, polycarbonate, and polyurethane.</p>
<p><img src="images/stretcher.png" alt="" title="Picture shows stretcher to evacuate patients from hospital in case of fire etc. (Oldale Plastic's own product.)" /></p>
<p>Some thermosets have been introduced too. However, the flow characteristics of the resin are all important for successful processing, hence the ubiquity of polyethylene.

</p>
<p>These materials and technological advances, combined with well designed surface finishes and graphics, are leading to the rotamoulding process away from traditional solutions into new areas of product application.</p>
</div> 
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>