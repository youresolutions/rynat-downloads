<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['wraparound'], $ini_array['description']['wraparound'], $ini_array['keywords']['wraparound'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['wraparound']; ?></h1>

<p>
<span class="gallery"><a href="karting.php" title="Karting">Karting</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="chainlink.php" title="Chain Link">Chain Link</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="durapod.php" title="Durapod">Durapod</a></span>
</p>


<p>&nbsp;</p>
<h2>Bumper Systems</h2>
<p><img src="images/wraparound.gif" width="184" height="277" class="flt_lft" alt="" title="Bumper System" /></p>
<p>Bumper and side protectors manufactured in High Density Polyethylene. 
Material used is 1000 grade U.H.M.W not to be confused with much inferior cheaper grades. </p>
<p>All our bumpers and panels are machined in house using C.N.C routing machinery. 
Any quantity manufactured to any size and specification. </p>
<p>Bumper systems designed in house if required.
Gives excellent performance when used in conjunction with our "CHAINLINK" barrier system. </p> 
<p>All our products are designed and manufactured in our Sheffield Factory. 

</p>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>