<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");



require('wp_forms_lib.inc.php');

// before any HTML code is output
wp_copy_remote_to_local(
    'http://www.webpanel.co.uk/form/?username=kartingsafety&password=durapod&action=get+form+data+as+php&form_id=46',
    $_SERVER['DOCUMENT_ROOT'] . '/cache/wp_form.46.inc.php');
wp_copy_remote_to_local(
    'http://www.webpanel.co.uk/form/wp_form_processor.inc.php.txt',
    $_SERVER['DOCUMENT_ROOT'] . '/cache/wp_form_processor.inc.php');


// the next line can be commented out if you are not using ReCAPTCHA
// require('recaptchalib.php');
require('Snoopy-1.2.3/Snoopy.class.php');
require('cache/wp_form.46.inc.php');
require('cache/wp_form_processor.inc.php');



wp_form_processor(46, 'kartingsafety', 'durapod');


/* Document head */
head($ini_array['title']['enquiry_form'], $ini_array['description']['enquiry_form'], $ini_array['keywords']['enquiry_form'], $ini_array['client_name']['full'], $ini_array['client_name']['full'], $ini_array['web']['full']);
?>


<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<?php

// output the form

wp_form_display(46);

// output the rest of your HTML from here...

echo<<<EOF
</div>
EOF;
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>