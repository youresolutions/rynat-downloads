<?php

$wpform[46] = (object)array();
$wpform[46]->id                    = 46;
$wpform[46]->name                  = "Enquiry Form";
$wpform[46]->notify_emails         = 'info@kartingsafety.co.uk, andrew@oldaleplastics.co.uk';
$wpform[46]->notify_sms            = '';
$wpform[46]->recaptcha_public_key  = '';
$wpform[46]->recaptcha_private_key = '';
$wpform[46]->submit_label          = 'Submit Enquiry';
$wpform[46]->success_url           = 'http://www.kartingsafety.co.uk/thank-you.php';
$wpform[46]->feature_captcha       = true;
$wpform[46]->feature_as_name       = true;

$wpfield[428] = (object)array();
$wpfield[428]->id               = 428;
$wpfield[428]->form_id          = 46;
$wpfield[428]->position         = 1;
$wpfield[428]->label            = 'Name';
$wpfield[428]->input_type       = 'text';
$wpfield[428]->options          = "";
$wpfield[428]->required_message = "Please enter your name";
$wpfield[428]->special_field    = 'name';

$wpfield[429] = (object)array();
$wpfield[429]->id               = 429;
$wpfield[429]->form_id          = 46;
$wpfield[429]->position         = 2;
$wpfield[429]->label            = 'Organisation';
$wpfield[429]->input_type       = 'text';
$wpfield[429]->options          = "";
$wpfield[429]->required_message = "";
$wpfield[429]->special_field    = 'none';

$wpfield[430] = (object)array();
$wpfield[430]->id               = 430;
$wpfield[430]->form_id          = 46;
$wpfield[430]->position         = 3;
$wpfield[430]->label            = 'Address';
$wpfield[430]->input_type       = 'textarea';
$wpfield[430]->options          = "";
$wpfield[430]->required_message = "";
$wpfield[430]->special_field    = 'none';

$wpfield[431] = (object)array();
$wpfield[431]->id               = 431;
$wpfield[431]->form_id          = 46;
$wpfield[431]->position         = 4;
$wpfield[431]->label            = 'Country';
$wpfield[431]->input_type       = 'dropdown';
$wpfield[431]->options          = "United Kingdom\nAfghanistan\nAlbania\nAlgeria\nAmerican Samoa\nAndorra\nAngola\nAnguilla\nAntigua and Barbuda\nAPO/FPO\nArgentina\nArmenia\nAruba\nAustralia\nAustria\nAzerbaijan Republic\nBahamas\nBahrain\nBangladesh\nBarbados\nBelarus\nBelgium\nBelize\nBenin\nBermuda\nBhutan\nBolivia\nBosnia and Herzegovina\nBotswana\nBrazil\nBritish Virgin Islands\nBrunei Darussalam\nBulgaria\nBurkina Faso\nBurma\nBurundi\nCambodia\nCameroon\nCanada\nCape Verde Islands\nCayman Islands\nCentral African Republic\nChad\nChile\nChina\nColombia\nComoros\nCongo, Democratic Republic of the\nCongo, Republic of the\nCook Islands\nCosta Rica\nCote d Ivoire (Ivory Coast)\nCroatia, Republic of\nCyprus\nCzech Republic\nDenmark\nDjibouti\nDominica\nDominican Republic\nEcuador\nEgypt\nEl Salvador\nEquatorial Guinea\nEritrea\nEstonia\nEthiopia\nFalkland Islands (Islas Malvinas)\nFiji\nFinland\nFrance\nFrench Guiana\nFrench Polynesia\nGabon Republic\nGambia\nGeorgia\nGermany\nGhana\nGibraltar\nGreece\nGreenland\nGrenada\nGuadeloupe\nGuam\nGuatemala\nGuernsey\nGuinea\nGuinea-Bissau\nGuyana\nHaiti\nHonduras\nHong Kong\nHungary\nIceland\nIndia\nIndonesia\nIreland\nIsrael\nItaly\nJamaica\nJan Mayen\nJapan\nJersey\nJordan\nKazakhstan\nKenya Coast Republic\nKiribati\nKorea, South\nKuwait\nKyrgyzstan\nLaos\nLatvia\nLebanon\nLiechtenstein\nLithuania\nLuxembourg\nMacau\nMacedonia\nMadagascar\nMalawi\nMalaysia\nMaldives\nMali\nMalta\nMarshall Islands\nMartinique\nMauritania\nMauritius\nMayotte\nMexico\nMicronesia\nMoldova\nMonaco\nMongolia\nMontserrat\nMorocco\nMozambique\nNamibia\nNauru\nNepal\nNetherlands\nNetherlands Antilles\nNew Caledonia\nNew Zealand\nNicaragua\nNiger\nNigeria\nNiue\nNorway\nOman\nPakistan\nPalau\nPanama\nPapua New Guinea\nParaguay\nPeru\nPhilippines\nPoland\nPortugal\nPuerto Rico\nQatar\nRomania\nRussian Federation\nRwanda\nSaint Helena\nSaint Kitts-Nevis\nSaint Lucia\nSaint Pierre and Miquelon\nSaint Vincent and the Grenadines\nSan Marino\nSaudi Arabia\nSenegal\nSeychelles\nSierra Leone\nSingapore\nSlovakia\nSlovenia\nSolomon Islands\nSomalia\nSouth Africa\nSpain\nSri Lanka\nSuriname\nSvalbard\nSwaziland\nSweden\nSwitzerland\nSyria\nTahiti\nTaiwan\nTajikistan\nTanzania\nThailand\nTogo\nTonga\nTrinidad and Tobago\nTunisia\nTurkey\nTurkmenistan\nTurks and Caicos Islands\nTuvalu\nUganda\nUkraine\nUnited Arab Emirates\nUnited States\nUruguay\nUzbekistan\nVanuatu\nVatican City State\nVenezuela\nVietnam\nVirgin Islands (U.S.)\nWallis and Futuna\nWestern Sahara\nWestern Samoa\nYemen\nYugoslavia\nZambia\nZimbabwe";
$wpfield[431]->required_message = "";
$wpfield[431]->special_field    = 'none';

$wpfield[432] = (object)array();
$wpfield[432]->id               = 432;
$wpfield[432]->form_id          = 46;
$wpfield[432]->position         = 5;
$wpfield[432]->label            = 'Postcode';
$wpfield[432]->input_type       = 'text';
$wpfield[432]->options          = "";
$wpfield[432]->required_message = "";
$wpfield[432]->special_field    = 'none';

$wpfield[433] = (object)array();
$wpfield[433]->id               = 433;
$wpfield[433]->form_id          = 46;
$wpfield[433]->position         = 6;
$wpfield[433]->label            = 'Telephone';
$wpfield[433]->input_type       = 'text';
$wpfield[433]->options          = "";
$wpfield[433]->required_message = "Please enter a landline or mobile number that we can reach you on";
$wpfield[433]->special_field    = 'tel';

$wpfield[434] = (object)array();
$wpfield[434]->id               = 434;
$wpfield[434]->form_id          = 46;
$wpfield[434]->position         = 7;
$wpfield[434]->label            = 'Email';
$wpfield[434]->input_type       = 'text';
$wpfield[434]->options          = "";
$wpfield[434]->required_message = "Please enter your Email address";
$wpfield[434]->special_field    = 'email';

$wpfield[435] = (object)array();
$wpfield[435]->id               = 435;
$wpfield[435]->form_id          = 46;
$wpfield[435]->position         = 8;
$wpfield[435]->label            = 'Fax';
$wpfield[435]->input_type       = 'text';
$wpfield[435]->options          = "";
$wpfield[435]->required_message = "";
$wpfield[435]->special_field    = 'none';

$wpfield[436] = (object)array();
$wpfield[436]->id               = 436;
$wpfield[436]->form_id          = 46;
$wpfield[436]->position         = 9;
$wpfield[436]->label            = 'Your enquiry';
$wpfield[436]->input_type       = 'textarea';
$wpfield[436]->options          = "";
$wpfield[436]->required_message = "Please enter the enquiry description";
$wpfield[436]->special_field    = 'none';

$wpfield[437] = (object)array();
$wpfield[437]->id               = 437;
$wpfield[437]->form_id          = 46;
$wpfield[437]->position         = 10;
$wpfield[437]->label            = 'Please call me back';
$wpfield[437]->input_type       = 'dropdown';
$wpfield[437]->options          = "Anytime\nMorning\nAfternoon\nEvening\nNo Thanks";
$wpfield[437]->required_message = "";
$wpfield[437]->special_field    = 'none';

$wpfield[438] = (object)array();
$wpfield[438]->id               = 438;
$wpfield[438]->form_id          = 46;
$wpfield[438]->position         = 11;
$wpfield[438]->label            = 'Where did you hear about us?';
$wpfield[438]->input_type       = 'dropdown';
$wpfield[438]->options          = "Other\nAlready a customer\nSearch engine\nReferred by a friend\nMagazine/newspaper advert";
$wpfield[438]->required_message = "";
$wpfield[438]->special_field    = 'none';

?>