<?php

/*

    EmailAddressValidator Class
    http://code.google.com/p/php-email-address-validation/

    Released under New BSD license
    http://www.opensource.org/licenses/bsd-license.php
    
    Sample Code
    ----------------
    $validator = new EmailAddressValidator;
    if ($validator->check_email_address('test@example.org')) {
        // Email address is technically valid
    }

*/

class WPEmailAddressValidator {

    /**
     * Check email address validity
     * @param   strEmailAddress     Email address to be checked
     * @return  True if email is valid, false if not
     */
    public function check_email_address($strEmailAddress) {
        
        // If magic quotes is "on", email addresses with quote marks will
        // fail validation because of added escape characters. Uncommenting
        // the next three lines will allow for this issue.
        //if (get_magic_quotes_gpc()) { 
        //    $strEmailAddress = stripslashes($strEmailAddress); 
        //}

        // Control characters are not allowed
        if (preg_match('/[\x00-\x1F\x7F-\xFF]/', $strEmailAddress)) {
            return false;
        }

        // Check email length - min 3 (a@a), max 256
        if (!$this->check_text_length($strEmailAddress, 3, 256)) {
            return false;
        }

        // Split it into sections using last instance of "@"
        $intAtSymbol = strrpos($strEmailAddress, '@');
        if ($intAtSymbol === false) {
            // No "@" symbol in email.
            return false;
        }
        $arrEmailAddress[0] = substr($strEmailAddress, 0, $intAtSymbol);
        $arrEmailAddress[1] = substr($strEmailAddress, $intAtSymbol + 1);

        // Count the "@" symbols. Only one is allowed, except where 
        // contained in quote marks in the local part. Quickest way to
        // check this is to remove anything in quotes. We also remove
        // characters escaped with backslash, and the backslash
        // character.
        $arrTempAddress[0] = preg_replace('/\./'
                                         ,''
                                         ,$arrEmailAddress[0]);
        $arrTempAddress[0] = preg_replace('/"[^"]+"/'
                                         ,''
                                         ,$arrTempAddress[0]);
        $arrTempAddress[1] = $arrEmailAddress[1];
        $strTempAddress = $arrTempAddress[0] . $arrTempAddress[1];
        // Then check - should be no "@" symbols.
        if (strrpos($strTempAddress, '@') !== false) {
            // "@" symbol found
            return false;
        }

        // Check local portion
        if (!$this->check_local_portion($arrEmailAddress[0])) {
            return false;
        }

        // Check domain portion
        if (!$this->check_domain_portion($arrEmailAddress[1])) {
            return false;
        }

        // If we're still here, all checks above passed. Email is valid.
        return true;

    }

    /**
     * Checks email section before "@" symbol for validity
     * @param   strLocalPortion     Text to be checked
     * @return  True if local portion is valid, false if not
     */
    protected function check_local_portion($strLocalPortion) {
        // Local portion can only be from 1 to 64 characters, inclusive.
        // Please note that servers are encouraged to accept longer local
        // parts than 64 characters.
        if (!$this->check_text_length($strLocalPortion, 1, 64)) {
            return false;
        }
        // Local portion must be:
        // 1) a dot-atom (strings separated by periods)
        // 2) a quoted string
        // 3) an obsolete format string (combination of the above)
        $arrLocalPortion = explode('.', $strLocalPortion);
        for ($i = 0, $max = sizeof($arrLocalPortion); $i < $max; $i++) {
             if (!preg_match('.^('
                            .    '([A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]' 
                            .    '[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]{0,63})'
                            .'|'
                            .    '("[^\\\"]{0,62}")'
                            .')$.'
                            ,$arrLocalPortion[$i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks email section after "@" symbol for validity
     * @param   strDomainPortion     Text to be checked
     * @return  True if domain portion is valid, false if not
     */
    protected function check_domain_portion($strDomainPortion) {
        // Total domain can only be from 1 to 255 characters, inclusive
        if (!$this->check_text_length($strDomainPortion, 1, 255)) {
            return false;
        }
        // Check if domain is IP, possibly enclosed in square brackets.
        if (preg_match('/^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
           .'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}$/'
           ,$strDomainPortion) || 
            preg_match('/^\[(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])'
           .'(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])){3}\]$/'
           ,$strDomainPortion)) {
            return true;
        } else {
            $arrDomainPortion = explode('.', $strDomainPortion);
            if (sizeof($arrDomainPortion) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0, $max = sizeof($arrDomainPortion); $i < $max; $i++) {
                // Each portion must be between 1 and 63 characters, inclusive
                if (!$this->check_text_length($arrDomainPortion[$i], 1, 63)) {
                    return false;
                }
                if (!preg_match('/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|'
                   .'([A-Za-z0-9]+))$/', $arrDomainPortion[$i])) {
                    return false;
                }
                if ($i == $max - 1) { // TLD cannot be only numbers
                    if (strlen(preg_replace('/[0-9]/', '', $arrDomainPortion[$i])) <= 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Check given text length is between defined bounds
     * @param   strText     Text to be checked
     * @param   intMinimum  Minimum acceptable length
     * @param   intMaximum  Maximum acceptable length
     * @return  True if string is within bounds (inclusive), false if not
     */
    protected function check_text_length($strText, $intMinimum, $intMaximum) {
        // Minimum and maximum are both inclusive
        $intTextLength = strlen($strText);
        if (($intTextLength < $intMinimum) || ($intTextLength > $intMaximum)) {
            return false;
        } else {
            return true;
        }
    }

}








$wp_form_errors = array();

function wp_form_htmlentities($string) {
  return htmlentities($string, ENT_COMPAT, 'UTF-8');
}

function wp_form_processor($form_id, $username, $password) {
    global $wpform, $wpfield;

    if(!isset($wpform[$form_id])) {
        echo '<!-- no form data -->';
        return;
    }


    if(!isset($_POST['wpf_submit'])) {
        return;
    }
    
    global $wp_form_errors;
    foreach($wpfield as $f) {
        if($f->form_id==$form_id) {
            $var = 'wpff' . $f->id;
            if(!empty($f->required_message) && (!isset($_POST[$var]) || empty($_POST[$var]))) {
                $wp_form_errors[] = $f->required_message;
            }
        }
    }

    // check reCAPTCHA field
    if(!empty($wpform[$form_id]->recaptcha_private_key) && $wpform[$form_id]->feature_captcha=='yes') {
        $resp = recaptcha_check_answer(
            $wpform[$form_id]->recaptcha_private_key,
            $_SERVER['REMOTE_ADDR'],
            $_POST['recaptcha_challenge_field'],
            $_POST['recaptcha_response_field']);
        if(!$resp->is_valid) {
            $wp_form_errors[] = 'The reCAPTCHA wasn\'t entered correctly. Please try again.';
            //  '(reCAPTCHA said: ' . $resp->error . ')';
        }
    }

    if(!empty($wp_form_errors)) {
        return;
    }



    $snoopy = new Snoopy();

    $formfiles = array();
    
    $formvars = array();
    $formvars['username'] = $username;
    $formvars['password'] = $password;
    $formvars['form_id'] = $form_id;
    $formvars['form_text'] = '';
    
    if(function_exists('wp_form_hook_form_text_pre')) {
      $formvars['form_text'] .= wp_form_hook_form_text_pre();
    }
    
    $formvars['form_text'] .= 'The following form was submitted from your website:' . "\n\n";

    $from = 'ianf@yesl.co.uk'; // default

    foreach($wpfield as $f) {
        if($f->input_type=='heading' || $f->input_type=='html') {
          continue;
        }
        if($f->form_id==$form_id) {
            $var = 'wpff' . $f->id;
            
            if($f->input_type=='file') {
                $snoopy->set_submit_multipart();
                if($_FILES[$var]['size']) {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/cache/';
                    $uploadfile = $uploaddir . basename($_FILES[$var]['name']);

                    if(@move_uploaded_file($_FILES[$var]['tmp_name'], $uploadfile)) {
                        $formfiles[$var] = $uploadfile;
                        $formvars['form_text'] .= $f->label . ': ' . basename($_FILES[$var]['name']) . "\n";
                    }
                }
            } else {
              if(!isset($_POST[$var])) {
                // unchecked checkboxes won't by POSTed
                continue;
              }
                $formvars['form_text'] .= $f->label . ': ' . $_POST[$var] . "\n";
                if($f->special_field=='name') {
                    $formvars['sender_name'] = $_POST[$var];
                } else if($f->special_field=='email') {
                    $formvars['email'] = $_POST[$var]; // security check?
                    $validator = new WPEmailAddressValidator;
                    if ($validator->check_email_address($_POST[$var])) {
                        $from = $_POST[$var];
                    }
                } else if($f->special_field=='tel') {
                    $formvars['form_text'] .= 'Watch out for scams. Is the number above premium rate?' . "\n";
                }
            }
        }
    }
    
    // check 1st letter of name anti-spam measure
    if($wpform[$form_id]->feature_as_name) {
        $check_letter = strtolower($_POST['as_name']);
        $from_name = strtolower(substr($formvars['sender_name'], 0, 1));
        if($from_name != $check_letter) {
            $wp_form_errors[] = 'Please re-enter the first letter of your name correctly';
            return;
        }
    }

    $to = $wpform[$form_id]->notify_emails;
    if(function_exists('wp_form_hook_to')) {
      $to = wp_form_hook_to();
    }
    $formvars['send_webpanel_notifications'] = 'yes';
    if(function_exists('wp_form_hook_send_webpanel_notifications')) {
      $formvars['send_webpanel_notifications'] = wp_form_hook_send_webpanel_notifications();
    }
    

    // send to notification emails
    
    if(!mail($to,
        'Form submission: ' . $wpform[$form_id]->name,
        $formvars['form_text'],
        'From: ' . $from)) {
        die('Could not send form!');
    }

    // submit to Webpanel form archive

    $uri = 'http://www.webpanel.co.uk/form/post.php';
    
    if(empty($formfiles)) {
        $snoopy->submit($uri, $formvars);
    } else {
        $snoopy->submit($uri, $formvars, $formfiles);
    }

    if(strpos($snoopy->results, 'SUCCESS')===false) {
        @mail('ianf@yesl.co.uk', 'Form failure', 'Form id: ' . $form_id . "\nError: " . $snoopy->results, 'From: ianf@yesl.co.uk');
        die('Sorry there was a problem sending the form. The website owner has been notified.');
        // alert?
    }




    header('Location: ' . $wpform[$form_id]->success_url);
    exit;
}

function wp_prepare_options($options) {
    $options = str_replace("\r", '', $options);
    $options = explode("\n", $options);
    return $options;
}

function wp_form_field_has_label_first($f) {
  $inputs_with_labels_first = array('search', 'tel', 'text', 'textarea', 'radio', 'dropdown', 'listbox', 'file', 'email', 'url');
  return in_array($f->input_type, $inputs_with_labels_first);
}

function wp_form_display($form_id)
{
    global $wpform, $wpfield;

    if(!isset($wpform[$form_id])) {
        echo '<!-- no form data -->';
        return;
    }


    
    echo '<div class="wpform">' . "\n";
    echo '<h1>' . $wpform[$form_id]->name . "</h1>\n";

    global $wp_form_errors;
    if(!empty($wp_form_errors)) {
        echo '<div class="wp_form_errors">';
        echo '<p>Please complete the form:</p>';
        echo '<ul>';
        foreach($wp_form_errors as $error) {
            echo '<li>' . $error . '</li>' . "\n";
        }
        echo '</ul>';
        echo '</div>';
    }

    $file_uploads = false;
    foreach($wpfield as $f) {
        if($f->form_id==$form_id && $f->input_type=='file') {
            $file_uploads = true;
        }
    }

    echo '<form ';
    if($file_uploads) {
        echo 'enctype="multipart/form-data" ';
    }
    echo 'id="wpf_' . $form_id . '" action="" method="post" onsubmit="return wpf_validate();">' . "\n";

    echo '<dl>' . "\n";
    foreach($wpfield as $f) {
        if($f->form_id==$form_id) {
            if($f->input_type=='heading') {
              echo '</dl><h2>' . wp_form_htmlentities($f->label) . "</h2><dl>\n";
              continue;
            } else if($f->input_type=='html') {
              echo '</dl>' . $f->options . "<dl>\n";
              continue;
            }
            $var = 'wpff' . $f->id;
            if(wp_form_field_has_label_first($f)) {
              echo '<dt class="' . $var . '">';
              echo '<label for="' . $var . '">';
              echo $f->label;
              if(!empty($f->required_message)) {
                  echo ' <span class="wpfrequired">*</span>';
              }
              echo '</label>';
            } else {
              echo '<dt class="wpfnolabel">';
              echo '&nbsp;';
            }
            echo '</dt>' . "\n";
            echo '<dd class="' . $var . '">';
            switch($f->input_type) {
            case 'file':
                echo '<input type="file" id="' . $var . '" name="' . $var . '" />';
                break;
            case 'radio':
                $options = wp_prepare_options($f->options);
                $first = true;
                foreach($options as $option) {
                    echo '<label class="wpfradio"><input type="radio" name="' . $var .
                        '" ';
                    if($first) {
                        echo 'id="' . $var . '" ';
                        $first = false;
                    }
                    echo 'value="' . wp_form_htmlentities($option) . '" /> ' . wp_form_htmlentities($option) .
                        "</label>\n";
                }
                break;
            case 'email':
            case 'search':
            case 'tel':
            case 'text':
            case 'url':
            case 'hidden':
                echo '<input type="' . $f->input_type . '" id="' . $var . '" name="' . $var . '" ';
                if(isset($_REQUEST[$var])) {
                    echo 'value="' . wp_form_htmlentities($_REQUEST[$var]) . '" ';
                }
                echo '>';
                break;
            case 'textarea':
                echo '<textarea rows="4" cols="40" id="' . $var . '" name="' . $var . '">';
                if(isset($_POST[$var])) {
                    echo wp_form_htmlentities($_POST[$var]);
                } else if(isset($_GET[$var])) {
                    echo wp_form_htmlentities($_GET[$var]);
                }
                echo '</textarea>';
                break;
            case 'dropdown':
                echo '<select id="' . $var . '" name="' . $var . '">';
                $options = wp_prepare_options($f->options);
                foreach($options as $option) {
                    echo '<option value="' . wp_form_htmlentities($option) . '">' . wp_form_htmlentities($option) . "</option>\n";
                }
                echo '</select>';
                break;
            case 'checkbox':
              echo '<label><input type="checkbox" id="' . $var . '" name="' . $var . '" ';
              if(isset($_POST[$var])) {
                echo 'checked="checked" ';
              }
              echo '/> ' . wp_form_htmlentities($f->label) . '</label>';
              break;
            }
            echo '</dd>' . "\n";
        }
    }

    if($wpform[$form_id]->feature_as_name) {
        echo '<dt>Please re-enter the first letter of your name <span class="wpfrequired">*</span>:</dt>';
        echo '<dd><input type="text" name="as_name" size="3" maxlength="3" /></dd>';
    }
  
    if(!empty($wpform[$form_id]->recaptcha_public_key) && $wpform[$form_id]->feature_captcha) {
        echo '<dt>Help prevent spam <span class="wpfrequired">*</span>:</dt>';
        echo '<dd>';
        echo recaptcha_get_html($wpform[$form_id]->recaptcha_public_key);
        echo '</dd>';
    }
    
    echo '<dt class="wpfsubmit">&nbsp;</dt>';
    echo '<dd class="wpfsubmit"><input type="submit" name="wpf_submit" value="';
    echo wp_form_htmlentities($wpform[$form_id]->submit_label);
    echo '" /></dd>' . "\n";
    
    echo '</dl>' . "\n";
    
    echo '</form>' . "\n";
    
    echo '</div><!--class=wpform-->' . "\n";
}

?>