<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['chainlink'], $ini_array['description']['chainlink'], $ini_array['keywords']['chainlink'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['chainlink']; ?></h1>
<p>
<span class="gallery"><a href="karting.php" title="Karting">Karting</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="wraparound.php" title="Wraparound">Wraparound</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="durapod.php" title="Durapod">Durapod</a></span>
</p>

<p>&nbsp;</p>
<h2>Track Barrier System<br />
Affordable Professional and Safe</h2>
<p><img src="images/chainlink_top.jpg" width="204" height="155" class="flt_rth" alt="" title="Track Barrier System" /></p>
<ul>
<li>Designed to improve security and safety for <strong>drivers</strong>, <strong>spectators</strong> and <strong>staff</strong></li>
<li>Quick assembly and positioning of system</li>
<li>Very <strong>heavy duty system</strong></li>
<li>Two heights available Mini 370mm high and Maxi 590mm high</li>
<li>Helps stop the &quot;boomerang&quot; effect to the kart when hit and creates an &quot;anti-embedding surface&quot;</li>
<li>Saves staff time helping drivers out of the normal tyre wall crashes</li>
<li>Linked system make moving the track course layout quick and easy compared with other systems</li>
<li>Reduces impact damage to karts particularly when used in conjunction with our &quot;WRAPAROUND&quot; products</li>
<li>Manufactured in virgin quality <strong>low density polyethylene</strong> not to be confused with inferior quality material</li>
<li>Excellent impact strength</li>
<li>Material gives good UV resistance</li>
<li>Any colour subject to quantity</li>
<li>Affordable professional looking system</li>
<li>Heavy-duty design and material thickness</li>
<li>Optional polyethylene buffer strip available inc. all fasteners and tools for a professional installation</li>
<li>Barrier makes a fantastic advertising medium that can generate revenue</li>
<li>All our products are designed and manufactured in our Sheffield factory</li>
</ul>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>