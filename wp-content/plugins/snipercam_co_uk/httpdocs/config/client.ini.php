<?php

[client_name]
footer="Kart Protection Systems. "
bkmrk= "Kart Protection Systems" ;
full = "Kart Protection Systems" ; required
short = "Kart Protection Systems" ; required - if not a limited company, make the same as the full name above
abbrev = "Kart Protection Systems" ; optional

[contact_person]
; First two lines of postal address on Contact Us page
name = "" ; optional
job_title = "" ; optional

[address]
line_1 = "Unit 4 Holbrook Commerce Park" ; optional
line_2 = "Holbrook Close" ; optional
line_3 = "Holdbrook Industrial Estate" ; optional
line_4 = "Sheffield" ; optional*/
postcode = "S20 3FJ" ; optional

[phone_number]
call_now = "Call Us: 07855 295907" ; optional
telephone_1 = "07855 295907" ; optional
telephone_2 = "0114 248 1973" ; optional
mobile_1 = "" ; optional
mobile_2 = "" ; optional
fax_1 = "0114 248 7480" ; optional
fax_2 = "" ; optional

[email]
primary = "info@kartingsafety.co.uk" ; required
other = "" ; optional
notification = "automailer@yesl.co.uk" ; required

[web]
redirect="http://www.kartingsafety.co.uk/";
sites="http://www.kartingsafety.co.uk/";
full = "http://www.kartingsafety.co.uk/" ; required e.g. http://www.yesl.co.uk
short = "www.kartingsafety.co.uk/" ; required e.g. www.yesl.co.uk

[phrase]
slogan = "" ; optional
headline = "" ; optional
strapline = "" ; optional
tagline = "" ; optional
catch = "" ; optional
motto = "" ; optional

?>
