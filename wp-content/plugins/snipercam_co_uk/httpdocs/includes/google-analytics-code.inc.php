<?php

function google_analytics_code($uacct = '')
{
	if (empty($uacct)) return;
	echo "<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\"></script>\n";
	echo "<script type=\"text/javascript\">\n";
	echo "_uacct = \"".$uacct."\";\n";
	echo "urchinTracker();\n";
	echo "</script>\n";
}

?>