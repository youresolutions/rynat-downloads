<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['routingservices'], $ini_array['description']['routingservices'], $ini_array['keywords']['routingservices'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['routingservices']; ?></h1>
<p><img src="images/machine.jpg" class="flt_lft" alt="" title="C.N.C. routing Service" />In keeping with our policy to be in the forefront of technology, Kart Protection Systems uses a state of the art Computerised Numerical Control (CNC) milling and routing facility which accurately machines full three dimensional products from drawings or a sample workpiece on either its original or modified format.
</p>

<p>Our CNC profiling routing bed has a 2700 x 915mm capacity for all plastics sheet materials and smaller rotational moulded products.

Production can be from a one off to thousands.

Finishing, deburring and second operations are all carried out in-house.
</p>
<div class="imgcontainer">
<p class="centertext">
<img src="images/cnc1.jpg" alt="" title="C.N.C. routing Service" /> <img src="images/cnc2.jpg" alt="" title="C.N.C. routing Service" /> <img src="images/cnc3.jpg" alt="" title="C.N.C. routing Service" /> 
</p>
</div>	
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>