<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['durapod'], $ini_array['description']['durapod'], $ini_array['keywords']['durapod'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['durapod']; ?></h1>
<p>
<span class="gallery"><a href="karting.php" title="Karting">Karting</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="chainlink.php" title="Chain Link">Chain Link</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="wraparound.php" title="Wraparound">Wraparound</a></span>
</p>

<p>&nbsp;</p>
<h2>Deformable Side Pods</h2>
<p><img src="images/durapod_top.jpg" class="flt_rth" alt="" title="Deformable Side Pods" /></p>
<ul>
<li>Supa-tough deformable design springs back into shape when hit</li>
<li>Durable-made from the same material as give way traffic bollards</li>
<li>Design features top and bottom solid rib to resist wear if in contact with ground</li>
<li>Choice of flat or curved outer surface available</li>
<li>Can be manufactured in different colours</li>
<li>Can be manufactured in hard none deformable material</li>
<li>Aerodynamic pro-kart styling</li>
<li>All our products are designed and manufactured in our Sheffield factory</li>
</ul></div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>