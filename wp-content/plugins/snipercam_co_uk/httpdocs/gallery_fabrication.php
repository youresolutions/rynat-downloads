<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['gallery'], $ini_array['description']['gallery'], $ini_array['keywords']['gallery'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Product Gallery</h1>
<p><a href="fabrication.php" class="gallery">Designers And Fabricatiors</a></p>
<div class="imgcontainer">
<div id="ga1"><img title="Picture shows Installation on Roof Of Water Tank." class="img" src="images/gal1/crane.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/crane_lg.jpg&amp;title=Picture shows Installation on Roof Of Water Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Sea water De-Salination Storage Tanks Saudi Arabia." class="img" src="images/gal1/sea_water.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/sea_water_lg.jpg&amp;title=Picture shows Sea water De-Salination Storage Tanks Saudi Arabia.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows 25mm Thick Wire Pickling Tank." class="img" src="images/gal1/pickling_tank.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/crane_lg.jpg&amp;title=Picture shows 25mm Thick Wire Pickling Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Storage Tanks Complete With Splash Proof Polycarbonate Enclosure." class="img" src="images/gal1/storage_tank.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/storage_tank_lg.jpg&amp;title=Picture shows Storage Tanks Complete With Splash Proof Polycarbonate Enclosure.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows University Lab, Fume Cupboard Extraction." class="img" src="images/gal1/university.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/university_lg.jpg&amp;title=Picture shows University Lab, Fume Cupboard Extraction.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Forty 40mm Thick Polypropylene Wire Pickling Tank." class="img" src="images/gal1/forty_pickling_tank.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/forty_pickling_tank_lg.jpg&amp;title=Picture shows Forty 40mm Thick Polypropylene Wire Pickling Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="PPicture shows Cylindrical PolyPropylene Water Tank." class="img" src="images/gal1/cylindrical.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/cylindrical_lg.jpg&amp;title=PPicture shows Cylindrical PolyPropylene Water Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="PVC Fume Cabinets University" class="img" src="images/gal1/pvc_university.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/pvc_university_lg.jpg&amp;title=PVC Fume Cabinets University','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows ?." class="img" src="images/gal1/welding.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/welding_lg.jpg&amp;title=Picture shows ?.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows On Site Installation of.Acid Plant Drainage Pipwork." class="img" src="images/gal1/acid_plant.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/acid_plant_lg.jpg&amp;title=Picture shows On Site Installation of.Acid Plant Drainage Pipwork.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows 20mm Thick Polypropylene Electro - Plating Tank." class="img" src="images/gal1/electroplating.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/electroplating_lg.jpg&amp;title=Picture shows 20mm Thick Polypropylene Electro - Plating Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows >." class="img" src="images/gal1/steel.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/steel_lg.jpg&amp;title=Picture shows >.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Wash Pool Fume Scrubber." class="img" src="images/gal1/washpool.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/washpool_lg.jpg&amp;title=Picture shows Wash Pool Fume Scrubber.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows H.D.P.E Bleach Tank." class="img" src="images/gal1/bleachtank.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/bleachtank_lg.jpg&amp;title=Picture shows H.D.P.E Bleach Tank.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Polypropylene High Pressure Wash Cabinets." class="img" src="images/gal1/highpressure.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/highpressure_lg.jpg&amp;title=Picture shows Polypropylene High Pressure Wash Cabinets.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows University Extraction Roof Discharge." class="img" src="images/gal1/roofdischarge.jpg" alt="Description of what the photo is of, and where it was taken.
" style="cursor:pointer" onClick="window.open('largview.php?img=images/gal1/roofdischarge_lg.jpg&amp;title=Picture shows University Extraction Roof Discharge.','','width=725,height=650,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> </div>
</div>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>