<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['karting'], $ini_array['description']['karting'], $ini_array['keywords']['karting'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['karting']; ?></h1>

<p>
<span class="gallery"><a href="chainlink.php" title="Chain Link">Chain Link</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="wraparound.php" title="Wraparound">Wraparound</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="durapod.php" title="Durapod">Durapod</a></span>
</p>

<p>&nbsp;</p>
<p><img src="images/durapod_colour.jpg" class="flt_rth" alt="" title="Different types and colors of Durapod" /></p>
<p>Karting Protection Systems was formed as a division of Kart Protection Systems in 1997 to service the growing demand for plastic products for the corporate karting Market.</p>

<p>We are the only business in the United Kingdom to supply the whole range of corporate kart protection systems and manufacture them all in house,</p>

<p>We supply only to the karting industry including kart tracks and most kart manufacturers and distributors.</p>

<p>We pride ourselves in offering only the best products manufactured from the very best materials.</p>

<p>Should you not see the product you require please give us a call and we will try our best to help.

</p>

</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>