<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['kartnewspage'], $ini_array['description']['kartnewspage'], $ini_array['keywords']['kartnewspage'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['kartnewspage']; ?></h1>

<p>
<span class="gallery"><a href="karting.php" title="Karting">Karting</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="chainlink.php" title="Chain Link">Chain Link</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="wraparound.php" title="Wraparound">Wraparound</a></span>
<img src="images/bullet.gif" class="lft" alt="|" />
<span class="gallery"><a href="durapod.php" title="Durapod">Durapod</a></span>
</p>

<p>&nbsp;</p>
<h2>New products for September 2008 include:</h2>
<ul><li><strong>Deformable moulded front bumper</strong></li></ul>

<p>If you have an interest in any of the new products or feel that a certain feature may benefit you please let us know now before we are committed to tooling up for the new products.</p>

<p>We want to give you the products that you require so please feel free to have an input.</p>

<p>Feel free to <a href="contact-us.php">contact us</a> should you require a product we don't yet do.
We are always prepared to consider any new products and design ideas.

</p>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>