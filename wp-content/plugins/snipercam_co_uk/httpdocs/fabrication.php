<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['fabrication'], $ini_array['description']['fabrication'], $ini_array['keywords']['fabrication'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['fabrication']; ?></h1>
<p><strong><a href="gallery.php#gallery2" class="gallery" title="Quick Link To Product Gallery">Quick Link To Product Gallery</a></strong></p>
<p><strong>Our Products and Services Include:</strong>
</p>
<img src="images/washbox.jpg" class="flt_rth" alt="" title="Wash Box Fume Scrubber" />
<ul>
<li>Cylindrical tanks for water and chemicals</li>
<li>Square or rectangular tanks</li>
<li>Drip trays</li>
<li>Fume extract ducting</li>
<li>Hoods</li>
<li>Instrument panels</li>
<li>Machine guards</li>
<li>Filters</li>
<li>Filter laterals</li>
<li>Cabinets and fume cupboards</li>
<li>All site work undertaken</li>
<li>Bund tanks and re linings</li>
<li>Electro plating plant</li>
<li>Fume scrubbers</li>
<li>Plastic pipework systems</li>
<li>24hr Call out</li>
</ul>
<p>The above list shows just some of the range of general fabrications in which Kart Protection Systems Specialise. Any one of these items or products specified by a client can be singly or mass produced.</p>

<p>A full consultancy service is available to all clients who would like advice on any fabrication required, and of course the same high standards of manufacturing are applied to all fabrications undertaken by Kart Protection Systems
</p>
<ul>
<li>Design</li>
<li>Manufacture</li>
<li>Installation</li>
<li>Maintenance</li>
</ul>
<p>Quality plastic fabrications in the following materials.</p>
<ul>
<li>Polypropylene</li>
<li>P.V.C</li>
<li>P.V.D.F</li>
<li>G.R.P</li>
<li>Polyethlene</li>
<li>Polycarbonate</li>
<li>Acrylic</li>
<li>and many others.</li>
</ul>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>