<?php

function wp_file_get_contents($url)
{
    $url = substr($url, 7);
    $domain = substr($url, 0, strpos($url, "/"));
    $path = substr($url, strpos($url, "/"));
    $f = @fsockopen($domain, 80, $errno, $errstr, 12);
    if($f===FALSE) {
        echo '!!!';
        return FALSE;
    }
    fputs($f, "GET $path HTTP/1.0\r\n");
    fputs($f, "Host: $domain\r\n");
    fputs($f, "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)\r\n\r\n");

    $c = '';
    while (!feof($f)) {
        $c .= fread($f, 8192);
    }
    fclose($f);
    return substr($c, strpos($c, "\r\n\r\n")+4); // skip HTTP response headers
}

function wp_copy_remote_to_local($remote_url, $local_path)
{
    $cache_timeout = 10;
    
    if(!file_exists($local_path) || filemtime($local_path) < (time()-$cache_timeout)) {
        $data = wp_file_get_contents($remote_url);
        if(FALSE===$data) {
            return FALSE;
        }
        $tmpf = tempnam('/tmp', 'YWS');
        $fp = fopen($tmpf, 'w');
        fwrite($fp, $data);
        fclose($fp);
        rename($tmpf, $local_path);
        chmod($local_path, 0666);
    }
}

?>