<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['rotanewspage'], $ini_array['description']['rotanewspage'], $ini_array['keywords']['rotanewspage'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1><? echo $ini_array['title']['rotanewspage']; ?></h1>
<p><a href="moulding.php" title="What is Rotational Moulding?" class="gallery">What is Rotational Moulding?</a> <img src="images/bullet.gif" alt="|"> <a href="moulds.php" title="The Moulds &amp; Materials"  class="gallery"> The Moulds &amp; Materials</a> <img src="images/bullet.gif"> <a href="design.php" title="Design &amp; Applecation" class="gallery">Design &amp; Application</a> <img src="images/bullet.gif"> <a href="gallery.php" title="Product Gallery" class="gallery">Product Gallery</a></p>
<p>Do you have products or mould tools that are surplus to requirements that you wish to sell?</p>

<p>If so we could be intrested in buying them any product considered.</p>

<p>Please <a href="contact-us.php">contact us</a> in complete confidence.</p>
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>