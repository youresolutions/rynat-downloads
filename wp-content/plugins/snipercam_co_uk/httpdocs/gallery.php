<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['gallery'], $ini_array['description']['gallery'], $ini_array['keywords']['gallery'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Product Gallery</h1>
<h2 id="gallery1">Karting Product Gallery</h2>
<div class="imgcontainer">
<img title="Picture shows aperture machining and trimming using special jigs made for each specific customer product after moulding." class="img cursor" src="images/gal2/jig.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/jig_lg.jpg&amp;title=Picture shows aperture machining and trimming using special jigs made for each specific customer product after moulding.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows hygienic plastic pallets." class="img cursor" src="images/gal2/pallets.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/pallets_lg.jpg&amp;title=Picture shows hygienic plastic pallets.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows flip top lid bin moulding." class="img cursor" src="images/gal2/bin.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/bin_lg.jpg&amp;title=Picture shows flip top lid bin moulding.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows dash board moulding." class="img cursor" src="images/gal2/dashboard.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/dashboard_lg.jpg&amp;title=Picture shows dash board moulding.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />


<img title="Picture shows engine transportation packaging." class="img cursor" src="images/gal2/engine.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/engine_lg.jpg&amp;title=Picture shows engine transportation packaging.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows stretcher to evacuate patients from hospital in case of fire etc." class="img cursor" src="images/gal2/stretcher.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/stretcher_lg.jpg&amp;title=Picture shows stretcher to evacuate patients from hospital in case of fire etc.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows traffic crash barrier (Oldale Plastic's own product)." class="img cursor" src="images/gal2/traffic.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/traffic_lg.jpg&amp;title=Picture shows engine transportation packaging.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows intermediate bulk container (IBC)." class="img cursor" src="images/gal2/bulk_container.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/bulk_container_lg.jpg&amp;title=Picture shows intermediate bulk container (IBC).','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows in ground drainage chamber." class="img cursor" src="images/gal2/drainage_chamber.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/drainage_chamber_lg.jpg&amp;title=Picture shows in ground drainage chamber.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows cab interior heater control housing." class="img cursor" src="images/gal2/heater_housing.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/heater_housing_lg.jpg&amp;title=Picture shows cab interior heater control housing.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />



<img title="Picture shows childrens play equipment: vending machine casing and artificial plastic rock." class="img cursor" src="images/gal2/childrens_play.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/childrens_play_lg.jpg&amp;title=Picture shows childrens play equipment: vending machine casing and artificial plastic rock.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows water tank c/w mounting points." class="img cursor" src="images/gal2/watertank.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/watertank_lg.jpg&amp;title=Picture shows water tank c/w mounting points.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows intermediate bulk container c/w pallet and side protection panels." class="img cursor" src="images/gal2/protection.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/protection_lg.jpg&amp;title=Picture shows intermediate bulk container c/w pallet and side protection panels.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />

<img title="Picture shows machine cover." class="img cursor" src="images/gal2/machine_cover.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/machine_cover_lg.jpg&amp;title=Picture shows machine cover.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />


<img title="Picture shows cab interior moulding." class="img cursor" src="images/gal2/cab_interior.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/cab_interior_lg.jpg&amp;title=Picture shows cab interior moulding.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />



<img title="Picture shows complete set of electric wheel chair mouldings." class="img cursor" src="images/gal2/wheelchair.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/wheelchair_lg.jpg&amp;title=Picture shows complete set of electric wheel chair mouldings.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />


<img title="Picture shows garage forecourt windscreen wash station (Oldale Plastic's own product)." class="img cursor" src="images/gal2/windscrren_wash.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/windscrren_wash_lg.jpg&amp;title=Picture shows garage forecourt windscreen wash station .','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />





<img title="Picture shows garage forecourt bin incorporating glove and hand towel dispenser. (Oldale Plastic's own product)." class="img cursor" src="images/gal2/towel_dispenser.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/towel_dispenser_lg.jpg&amp;title=Picture shows garage forecourt bin incorporating glove and hand towel dispenser.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
<img title="Picture shows childs play rockers (Oldale Plastic's own product)." class="img cursor" src="images/gal2/rockers.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/rockers_lg.jpg&amp;title=Picture shows childs play rockers.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
<img title="Picture shows go-kart side protection pod." class="img cursor" src="images/gal2/protection_pod.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal2/protection_pod_lg.jpg&amp;title=Picture shows go-kart side protection pod.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" />
</div>
<!--
<h2 id="gallery2">Welding &amp; Fabrication Product Gallery</h2>
<div class="imgcontainer">
<div id="ga1"><img title="Picture shows Installation on Roof Of Water Tank." class="img cursor" src="images/gal1/crane.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/crane_lg.jpg&amp;title=Picture shows Installation on Roof Of Water Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Sea water De-Salination Storage Tanks Saudi Arabia." class="img cursor" src="images/gal1/sea_water.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/sea_water_lg.jpg&amp;title=Picture shows Sea water De-Salination Storage Tanks Saudi Arabia.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows 25mm Thick Wire Pickling Tank." class="img cursor" src="images/gal1/pickling_tank.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/crane_lg.jpg&amp;title=Picture shows 25mm Thick Wire Pickling Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Storage Tanks Complete With Splash Proof Polycarbonate Enclosure." class="img cursor" src="images/gal1/storage_tank.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/storage_tank_lg.jpg&amp;title=Picture shows Storage Tanks Complete With Splash Proof Polycarbonate Enclosure.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows University Lab, Fume Cupboard Extraction." class="img cursor" src="images/gal1/university.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/university_lg.jpg&amp;title=Picture shows University Lab, Fume Cupboard Extraction.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Forty 40mm Thick Polypropylene Wire Pickling Tank." class="img cursor" src="images/gal1/forty_pickling_tank.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/forty_pickling_tank_lg.jpg&amp;title=Picture shows Forty 40mm Thick Polypropylene Wire Pickling Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Cylindrical PolyPropylene Water Tank." class="img cursor" src="images/gal1/cylindrical.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/cylindrical_lg.jpg&amp;title=Picture shows Cylindrical PolyPropylene Water Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="PVC Fume Cabinets University" class="img cursor" src="images/gal1/pvc_university.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/pvc_university_lg.jpg&amp;title=PVC Fume Cabinets University','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows On site butt welding of H.D.P.E pipework." class="img cursor" src="images/gal1/welding.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/welding_lg.jpg&amp;title=Picture shows On site butt welding of H.D.P.E pipework.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows On Site Installation of.Acid Plant Drainage Pipwork." class="img cursor" src="images/gal1/acid_plant.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/acid_plant_lg.jpg&amp;title=Picture shows On Site Installation of.Acid Plant Drainage Pipwork.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows 20mm Thick Polypropylene Electro - Plating Tank." class="img cursor" src="images/gal1/electroplating.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/electroplating_lg.jpg&amp;title=Picture shows 20mm Thick Polypropylene Electro - Plating Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows >." class="img cursor" src="images/gal1/steel.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/steel_lg.jpg&amp;title=Picture shows >.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Wash Pool Fume Scrubber." class="img cursor" src="images/gal1/washpool.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/washpool_lg.jpg&amp;title=Picture shows Wash Pool Fume Scrubber.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows H.D.P.E Bleach Tank." class="img cursor" src="images/gal1/bleachtank.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/bleachtank_lg.jpg&amp;title=Picture shows H.D.P.E Bleach Tank.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows Polypropylene High Pressure Wash Cabinets." class="img cursor" src="images/gal1/highpressure.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/highpressure_lg.jpg&amp;title=Picture shows Polypropylene High Pressure Wash Cabinets.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> <img title="Picture shows University Extraction Roof Discharge." class="img cursor" src="images/gal1/roofdischarge.jpg" alt="Description of what the photo is of, and where it was taken.
"  onclick="window.open('largview.php?img=images/gal1/roofdischarge_lg.jpg&amp;title=Picture shows University Extraction Roof Discharge.','','width=725,height=500,resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0')" /> </div>
</div>-->
</div>
<?php
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>