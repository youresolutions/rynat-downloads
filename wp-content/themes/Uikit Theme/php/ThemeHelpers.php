<?php
/**
 * Provides some helpers to use within the views.
 */
class ThemeHelpers
{
    /**
     * Display an optional post thumbnail.
     *
     * Wraps the post thumbnail in an anchor element on index
     * views, or a div element when on single views.
     */
    public function postThumbnail()
    {
        if (post_password_required() || is_attachment() || !has_post_thumbnail()) {
            return;
        }
        if (is_singular()) {
            echo '<a class="uk-thumbnail">';
            the_post_thumbnail();
            echo '</a>';
        } else {
            echo '<a class="uk-thumbnail" href="<?php the_permalink(); ?>">';
            the_post_thumbnail();
            echo '</a>';
        }
    }
    /**
     * Displays the pagination for the posts overview page (and search and archive)
     */
    public function getPostsPagination()
    {
        $pagination = paginate_links(array('type' => 'array', 'mid_size' => 3, 'prev_next' => false));
        if ($pagination == null) return;
        $returner = array();
        $returner[] = '<ul class="uk-pagination">';
        $returner[] = '<li class="uk-pagination-previous">' . get_previous_posts_link() . '</li>';
        for ($i = 0; $i < sizeof($pagination); $i++) {
            $returner[] = '<li class="uk-hidden-small">' . $pagination[$i] . '</li>';
        }
        $returner[] = '<li class="uk-pagination-next">' . get_next_posts_link() . '</li>';
        $returner[] = '</ul>';
        return implode('', $returner);
    }
}