<?php

/**
 * Master theme class
 *
 * @package Bolts
 * @since 1.0
 */
class My_Theme_Options {

	private $sections;
	private $checkboxes;
	private $settings;

	/**
	 * Construct
	 *
	 * @since 1.0
	 */
	public function __construct() {

		// This will keep track of the checkbox options for the validate_settings function.
		$this->checkboxes = array();
		$this->settings = array();
		$this->get_settings();

		$this->sections['general']      = __( 'General Settings' );
		$this->sections['front']		= __( 'Front Page');
		$this->sections['appearance']   = __( 'Custom CSS' );
		$this->sections['reset']        = __( 'Reset to Defaults' );
		$this->sections['about']        = __( 'About' );

		add_action( 'admin_menu', array( &$this, 'add_pages' ) );
		add_action( 'admin_init', array( &$this, 'register_settings' ) );

		if ( ! get_option( 'mytheme_options' ) )
			$this->initialize_settings();

	}

	/**
	 * Add options page
	 *
	 * @since 1.0
	 */
	public function add_pages() {

		$admin_page = add_theme_page( __( 'Theme Options' ), __( 'Theme Options' ), 'manage_options', 'mytheme-options', array( &$this, 'display_page' ) );

		add_action( 'admin_print_scripts-' . $admin_page, array( &$this, 'scripts' ) );
		add_action( 'admin_print_styles-' . $admin_page, array( &$this, 'styles' ) );

	}

	/**
	 * Create settings field
	 *
	 * @since 1.0
	 */
	public function create_setting( $args = array() ) {

		$defaults = array(
			'id'      => 'default_field',
			'title'   => __( 'Default Field' ),
			'desc'    => __( 'This is a default description.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'general',
			'choices' => array(),
			'class'   => ''
		);

		extract( wp_parse_args( $args, $defaults ) );

		$field_args = array(
			'type'      => $type,
			'id'        => $id,
			'desc'      => $desc,
			'std'       => $std,
			'choices'   => $choices,
			'label_for' => $id,
			'class'     => $class
		);

		if ( $type == 'checkbox' )
			$this->checkboxes[] = $id;

		add_settings_field( $id, $title, array( $this, 'display_setting' ), 'mytheme-options', $section, $field_args );
	}

	/**
	 * Display options page
	 *
	 * @since 1.0
	 */
	public function display_page() {

		echo '<div class="wrap">
	<div class="icon32" id="icon-options-general"></div>
	<h2>' . __( 'Theme Options' ) . '</h2>';

		if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] == true )
			echo '<div class="updated fade"><p>' . __( 'Theme options updated.' ) . '</p></div>';

		echo '<form action="options.php" method="post">';

		settings_fields( 'mytheme_options' );
		echo '<div class="ui-tabs">
			<ul class="ui-tabs-nav">';

		foreach ( $this->sections as $section_slug => $section )
			echo '<li><a href="#' . $section_slug . '">' . $section . '</a></li>';

		echo '</ul>';
		do_settings_sections( $_GET['page'] );

		echo '</div>
		<p class="submit"><input name="Submit" type="submit" class="button-primary" value="' . __( 'Save Changes' ) . '" /></p>

	</form>';

	echo '<script type="text/javascript">
		jQuery(document).ready(function($) {
			var sections = [];';

			foreach ( $this->sections as $section_slug => $section )
				echo "sections['$section'] = '$section_slug';";

			echo 'var wrapped = $(".wrap h3").wrap("<div class=\"ui-tabs-panel\">");
			wrapped.each(function() {
				$(this).parent().append($(this).parent().nextUntil("div.ui-tabs-panel"));
			});
			$(".ui-tabs-panel").each(function(index) {
				$(this).attr("id", sections[$(this).children("h3").text()]);
				if (index > 0)
					$(this).addClass("ui-tabs-hide");
			});
			$(".ui-tabs").tabs({
				fx: { opacity: "toggle", duration: "fast" }
			});

			$("input[type=text], textarea").each(function() {
				if ($(this).val() == $(this).attr("placeholder") || $(this).val() == "")
					$(this).css("color", "#999");
			});

			$("input[type=text], textarea").focus(function() {
				if ($(this).val() == $(this).attr("placeholder") || $(this).val() == "") {
					$(this).val("");
					$(this).css("color", "#000");
				}
			}).blur(function() {
				if ($(this).val() == "" || $(this).val() == $(this).attr("placeholder")) {
					$(this).val($(this).attr("placeholder"));
					$(this).css("color", "#999");
				}
			});

			$(".wrap h3, .wrap table").show();

			// This will make the "warning" checkbox class really stand out when checked.
			// I use it here for the Reset checkbox.
			$(".warning").change(function() {
				if ($(this).is(":checked"))
					$(this).parent().css("background", "#c00").css("color", "#fff").css("fontWeight", "bold");
				else
					$(this).parent().css("background", "none").css("color", "inherit").css("fontWeight", "normal");
			});

			// Browser compatibility
			if ($.browser.mozilla)
			         $("form").attr("autocomplete", "off");
		});
	</script>
</div>';

	}

	/**
	 * Description for section
	 *
	 * @since 1.0
	 */
	public function display_section() {
		// code
	}

	/**
	 * Description for About section
	 *
	 * @since 1.0
	 */
	public function display_about_section() {

		// This displays on the "About" tab. Echo regular HTML here, like so:
		// echo '<p>Copyright 2011 me@example.com</p>';
  echo '<p>This theme was created by Rebecca Chapman for Your E Solutions.</p>';

	}

	/**
	 * HTML output for text field
	 *
	 * @since 1.0
	 */
	public function display_setting(



		$args = array() ) {

		extract( $args );

		$options = get_option( 'mytheme_options' );

		if ( ! isset( $options[$id] ) && $type != 'checkbox' )
			$options[$id] = $std;
		elseif ( ! isset( $options[$id] ) )
			$options[$id] = 0;

		$field_class = '';
		if ( $class != '' )
			$field_class = ' ' . $class;

		switch ( $type ) {

			case 'heading':
				echo '</td></tr><tr valign="top"><td colspan="2"><h4>' . $desc . '</h4>';
				break;

			case 'areadescription':
				echo '<span class="description" style="float:left; z-index:99;">' . $desc . '</span>';
				break;


			case 'checkbox':

				echo '<input class="checkbox' . $field_class . '" type="checkbox" id="' . $id . '" name="mytheme_options[' . $id . ']" value="1" ' . checked( $options[$id], 1, false ) . ' /> <label for="' . $id . '">' . $desc . '</label>';

				break;

			case 'select':
				echo '<select class="select' . $field_class . '" name="mytheme_options[' . $id . ']">';

				foreach ( $choices as $value => $label )
					echo '<option value="' . esc_attr( $value ) . '"' . selected( $options[$id], $value, false ) . '>' . $label . '</option>';

				echo '</select>';

				if ( $desc != '' )
					echo '<br /><span class="description">' . $desc . '</span>';

				break;

			case 'radio':
				$i = 0;
				foreach ( $choices as $value => $label ) {
					echo '<input class="radio' . $field_class . '" type="radio" name="mytheme_options[' . $id . ']" id="' . $id . $i . '" value="' . esc_attr( $value ) . '" ' . checked( $options[$id], $value, false ) . '> <label for="' . $id . $i . '">' . $label . '</label>';
					if ( $i < count( $options ) - 1 )
						echo '<br />';
					$i++;
				}

				if ( $desc != '' )
					echo '<br /><span class="description">' . $desc . '</span>';

				break;

			case 'textarea':
				echo '<textarea class="' . $field_class . '" id="' . $id . '" name="mytheme_options[' . $id . ']" placeholder="' . $std . '" rows="5" cols="30">' . wp_htmledit_pre( $options[$id] ) . '</textarea>';

				if ( $desc != '' )
					echo '<br /><span class="description">' . $desc . '</span>';

				break;

			case 'password':
				echo '<input class="regular-text' . $field_class . '" type="password" id="' . $id . '" name="mytheme_options[' . $id . ']" value="' . esc_attr( $options[$id] ) . '" />';

				if ( $desc != '' )
					echo '<br /><span class="description">' . $desc . '</span>';

				break;

			case 'text':
			default:
		 		echo '<input class="regular-text' . $field_class . '" type="text" id="' . $id . '" name="mytheme_options[' . $id . ']" placeholder="' . $std . '" value="' . esc_attr( $options[$id] ) . '" />';

		 		if ( $desc != '' )
		 			echo '<br /><span class="description">' . $desc . '</span>';

		 		break;

		}

	}

	/**
	 * Settings and defaults
	 *
	 * @since 1.0
	 */
	public function get_settings() {

		/* General Settings
		===========================================*/

		$this->settings['uikit'] = array(
			'section' => 'general',
			'title'   => '', // Not used for headings.
			'desc'    => '<a href="http://getuikit.com/docs/customizer.html"><img src="http://firstaccountancy.yes1.co.uk/wp-content/uploads/2015/10/uikit-customizer-small.png"></a>',
			'type'    => 'heading'
		);

		$this->settings['fullscreen'] = array(
			'section' => 'general',
			'title'   => __( 'Fullscreen Mode' ),
			'desc'    => __( 'Do you want the website to span the full page?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['Custom_Width'] = array(
			'title'   => __( 'Custom Website Width' ),
			'desc'    => __( 'Choose a custom website width. Please note this will only work with fullscreen unselected.' ),
			'std'     => '1200',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['custombgcol'] = array(
			'title'   => __( 'Custom Background Colour' ),
			'desc'    => __( 'Choose a custom background colour. Please enter the hex value for your chosen colour.' ),
			'std'     => 'ffffff',
			'type'    => 'text',
			'section' => 'general'
		);


		$this->settings['twosidebars'] = array(
			'section' => 'general',
			'title'   => __( 'Sidebars on both sides?' ),
			'desc'    => __( 'Do you want a sidebar on the left and right? If left unselected only one sidebar will be displayed. To remove that, remove all widgets from it in the Widget section of WordPress.' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);


		$this->settings['sidebarloc'] = array(
			'section' => 'general',
			'title'   => __( 'Sidebar Location' ),
			'desc'    => __( 'Do you want the sidebar to be on the left side or the right side. This is irrelevant if you have sidebars enabled on both sides.' ),
			'type'    => 'radio',
			'std'     => 'left',
			'choices' => array(
				'left' => 'Left',
				'right' => 'Right'
			)
		);


		$this->settings['headerheading'] = array(
			'section' => 'general',
			'title'   => '', // Not used for headings.
			'desc'    => 'Header',
			'type'    => 'heading'
		);


		$this->settings['header'] = array(
			'section' => 'general',
			'title'   => __( 'Display Header Above Navbar?' ),
			'desc'    => __( 'Do you want to display a header above the navigation bar?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['headerheight'] = array(
			'title'   => __( 'Header Height' ),
			'desc'    => __( 'Enter the height you want the header to be in pixels.' ),
			'std'     => '140',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['headerlogo'] = array(
			'title'   => __( 'Header Logo' ),
			'desc'    => __( 'Enter the url for the logo you wish to be displayed in the header. Leave blank if you just want text and no logo.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['logoheight'] = array(
			'title'   => __( 'Header Logo Height' ),
			'desc'    => __( 'Enter the height you want the logo to be in pixels. By default this will keep the images original height.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['logowidth'] = array(
			'title'   => __( 'Header Logo Width' ),
			'desc'    => __( 'Enter the width you want the logo to be in pixels. By default this will keep the images original width.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['hexcolour'] = array(
			'section' => 'general',
			'title'   => 'Colours',
			'desc'    => 'Please enter the hex value for the colours you wish to use, you can get the hex value from <a href="http://www.w3schools.com/tags/ref_colorpicker.asp">here</a>.',
			'type'    => 'areadescription'
		);


		$this->settings['headercolour'] = array(
			'title'   => __( 'Header Background Colour' ),
			'desc'    => __( 'What background colour do you want your header to have if there is no image.' ),
			'std'     => 'ffffff',
			'type'    => 'text',
			'section' => 'general'
		);

		$this->settings['headertextcolour'] = array(
			'title'   => __( 'Header Text Colour' ),
			'desc'    => __( 'What text colour do you want your header to have.' ),
			'std'     => '444444',
			'type'    => 'text',
			'section' => 'general'
		);


		/* Front Page
		===========================================*/

		$this->settings['frontsidebar'] = array(
			'section' => 'front',
			'title'   => __( 'Sidebars on Front Page?' ),
			'desc'    => __( 'Do you want the front page to have any sidebars enabled?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);


		$this->settings['sliderheading'] = array(
			'section' => 'front',
			'title'   => '', // Not used for headings.
			'desc'    => 'Slider',
			'type'    => 'heading'
		);

		$this->settings['slider'] = array(
			'section' => 'front',
			'title'   => __( 'Fullscreen Slider' ),
			'desc'    => __( 'Do you want the front page to have a fullscreen slideshow?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['arrownav'] = array(
			'section' => 'front',
			'title'   => __( 'Arrow Navigation' ),
			'desc'    => __( 'Do you want the slider to have arrow navigation at the left and right side?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['kenburns'] = array(
			'section' => 'front',
			'title'   => __( 'Kenburns Animation Effect' ),
			'desc'    => __( '' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['autoplay'] = array(
			'section' => 'front',
			'title'   => __( 'Autoplay' ),
			'desc'    => __( '' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);


		$this->settings['slidernav'] = array(
			'section' => 'front',
			'title'   => __( 'Slideshow Nav' ),
			'desc'    => __( 'Whether the slideshow has any navigation at the bottom and if so what type.' ),
			'type'    => 'select',
			'std'     => 'none',
			'choices' => array(
				'none' => 'None',
				'dotnav' => 'Dotnav',
			)
		);

		$this->settings['anim'] = array(
			'section' => 'front',
			'title'   => __( 'Transition Animation' ),
			'desc'    => __( '' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'' => 'None',
				'animation: \'random-fx\'' => 'Random',
				'animation: \'slice-down\'' => 'Slice Down',
				'animation: \'slice-up\'' => 'Slice Up',
				'animation: \'slice-up-down\'' => 'Slice Up Down',
				'animation: \'fade\'' => 'Fade',
				'animation: \'scale\'' => 'Scale',
				'animation: \'scroll\'' => 'Scroll',
				'animation: \'swipe\'' => 'Swipe',
				'animation: \'fold\'' => 'Fold',
				'animation: \'puzzle\'' => 'Puzzle',
				'animation: \'boxes\'' => 'Boxes',
				'animation: \'boxes-reverse\'' => 'Boxes Reverse',

			)
		);

		$this->settings['overlaydisp'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Display' ),
			'desc'    => __( 'When should the overlay be displayed?' ),
			'type'    => 'select',
			'std'     => 'uk-overlay-active',
			'choices' => array(
				'uk-overlay-active' => 'Slide Active',
				'uk-overlay-hover' => 'On Hover',
			)
		);

		$this->settings['firstslide'] = array(
			'section' => 'front',
			'title'   => 'First Slide',
			'desc'    => '',
			'type'    => 'areadescription'
		);



		$this->settings['slideone'] = array(
			'title'   => __( 'Slide Image' ),
			'desc'    => __( 'Enter the Image you want on your first slide. Leave blank if not in use.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'front'
		);

		$this->settings['overlayone'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay on Slide?' ),
			'desc'    => __( 'Do you want an overlay on the slide?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['overlaydesone'] = array(
			'title'   => __( 'Overlay Text' ),
			'desc'    => __( 'Enter what you want to be displayed on the overlay, HTML can be used.' ),
			'std'     => '',
			'type'    => 'textarea',
			'section' => 'front',
			'class'   => 'code'
		);

		$this->settings['olbgone'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Background?' ),
			'desc'    => __( 'Display the overlay background?' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
		);

		$this->settings['overlaylocone'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Location' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-flex-middle uk-flex-center',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
		);

		$this->settings['textalignone'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Text Align' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-text-center',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
		);

		$this->settings['overlayanimone'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Animation' ),
			'desc'    => __( '' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
		);

		$this->settings['secondslide'] = array(
			'section' => 'front',
			'title'   => 'Second Slide',
			'desc'    => '<hr />',
			'type'    => 'areadescription'
		);

		$this->settings['slidetwo'] = array(
			'title'   => __( 'Slider Image 2' ),
			'desc'    => __( 'Enter the Image you want on your second slide. Leave blank if not in use.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'front'
		);

		$this->settings['overlaytwo'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay on Slide?' ),
			'desc'    => __( 'Do you want an overlay on the slide?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['overlaydestwo'] = array(
			'title'   => __( 'Overlay Text' ),
			'desc'    => __( 'Enter what you want to be displayed on the overlay, HTML can be used.' ),
			'std'     => '',
			'type'    => 'textarea',
			'section' => 'front',
			'class'   => 'code'
		);

		$this->settings['olbgtwo'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Background?' ),
			'desc'    => __( 'Display the overlay background?' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
		);

		$this->settings['overlayloctwo'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Location' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-flex-middle uk-flex-center',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
		);

		$this->settings['textaligntwo'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Text Align' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-text-center',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
		);

		$this->settings['overlayanimtwo'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Animation' ),
			'desc'    => __( '' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
		);

		$this->settings['thirdslide'] = array(
			'section' => 'front',
			'title'   => 'Third Slide',
			'desc'    => '',
			'type'    => 'areadescription'
		);

		$this->settings['slidethr'] = array(
			'title'   => __( 'Slider Image Three' ),
			'desc'    => __( 'Enter the Image you want on your third slide. Leave blank if not in use.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'front'
		);

		$this->settings['overlaythr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay on Slide?' ),
			'desc'    => __( 'Do you want an overlay on the slide?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['overlaydesthr'] = array(
			'title'   => __( 'Overlay Text' ),
			'desc'    => __( 'Enter what you want to be displayed on the overlay, HTML can be used.' ),
			'std'     => '',
			'type'    => 'textarea',
			'section' => 'front',
			'class'   => 'code'
		);

		$this->settings['olbgthr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Background?' ),
			'desc'    => __( 'Display the overlay background?' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
		);

		$this->settings['overlaylocthr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Location' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-flex-middle uk-flex-center',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
		);

		$this->settings['textalignthr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Text Align' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-text-center',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
		);

		$this->settings['overlayanimthr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Animation' ),
			'desc'    => __( '' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
		);

		$this->settings['fourthslide'] = array(
			'section' => 'front',
			'title'   => 'Fourth Slide',
			'desc'    => '',
			'type'    => 'areadescription'
		);

		$this->settings['slidefr'] = array(
			'title'   => __( 'Slider Image Four' ),
			'desc'    => __( 'Enter the Image you want on your fourth slide. Leave blank if not in use.' ),
			'std'     => '',
			'type'    => 'text',
			'section' => 'front'
		);

		$this->settings['overlayfr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay on Slide?' ),
			'desc'    => __( 'Do you want an overlay on the slide?' ),
			'type'    => 'checkbox',
			'std'     => 0 // Set to 1 to be checked by default, 0 to be unchecked by default.
		);

		$this->settings['overlaydesfr'] = array(
			'title'   => __( 'Overlay Text' ),
			'desc'    => __( 'Enter what you want to be displayed on the overlay, HTML can be used.' ),
			'std'     => '',
			'type'    => 'textarea',
			'section' => 'front',
			'class'   => 'code'
		);

		$this->settings['olbgfr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Background?' ),
			'desc'    => __( 'Display the overlay background?' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
		);

		$this->settings['overlaylocfr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Location' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-flex-middle uk-flex-center',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
		);

		$this->settings['textalignfr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Text Align' ),
			'desc'    => __( 'Where the overlay will be displayed.' ),
			'type'    => 'select',
			'std'     => 'uk-text-center',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
		);

		$this->settings['overlayanimfr'] = array(
			'section' => 'front',
			'title'   => __( 'Overlay Animation' ),
			'desc'    => __( '' ),
			'type'    => 'select',
			'std'     => '',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
		);




		/* Custom CSS
		===========================================*/


		$this->settings['custom_css'] = array(
			'title'   => __( 'Custom Styles' ),
			'desc'    => __( 'Enter any custom CSS here to apply it to your theme.' ),
			'std'     => '',
			'type'    => 'textarea',
			'section' => 'appearance',
			'class'   => 'code'
		);

		/* Reset
		===========================================*/

		$this->settings['reset_theme'] = array(
			'section' => 'reset',
			'title'   => __( 'Reset theme' ),
			'type'    => 'checkbox',
			'std'     => 0,
			'class'   => 'warning', // Custom class for CSS
			'desc'    => __( 'Check this box and click "Save Changes" below to reset theme options to their defaults.' )
		);



	}

	/**
	 * Initialize settings to their default values
	 *
	 * @since 1.0
	 */
	public function initialize_settings() {

		$default_settings = array();
		foreach ( $this->settings as $id => $setting ) {
			if ( $setting['type'] != 'heading' )
				$default_settings[$id] = $setting['std'];
		}

		update_option( 'mytheme_options', $default_settings );

	}

	/**
	* Register settings
	*
	* @since 1.0
	*/
	public function register_settings() {

		register_setting( 'mytheme_options', 'mytheme_options', array ( &$this, 'validate_settings' ) );

		foreach ( $this->sections as $slug => $title ) {
			if ( $slug == 'about' )
				add_settings_section( $slug, $title, array( &$this, 'display_about_section' ), 'mytheme-options' );
			else
				add_settings_section( $slug, $title, array( &$this, 'display_section' ), 'mytheme-options' );
		}

		$this->get_settings();

		foreach ( $this->settings as $id => $setting ) {
			$setting['id'] = $id;
			$this->create_setting( $setting );
		}

	}

	/**
	* jQuery Tabs
	*
	* @since 1.0
	*/
	public function scripts() {

		wp_print_scripts( 'jquery-ui-tabs' );

	}

	/**
	* Styling for the theme options page
	*
	* @since 1.0
	*/
	public function styles() {

		wp_register_style( 'mytheme-admin', get_bloginfo( 'stylesheet_directory' ) . '/mytheme-options.css' );
		wp_enqueue_style( 'mytheme-admin' );

	}

	/**
	* Validate settings
	*
	* @since 1.0
	*/
	public function validate_settings( $input ) {

		if ( ! isset( $input['reset_theme'] ) ) {
			$options = get_option( 'mytheme_options' );

			foreach ( $this->checkboxes as $id ) {
				if ( isset( $options[$id] ) && ! isset( $input[$id] ) )
					unset( $options[$id] );
			}

			return $input;
		}
		return false;

	}

}

$theme_options = new My_Theme_Options();

function mytheme_option( $option ) {
	$options = get_option( 'mytheme_options' );
	if ( isset( $options[$option] ) )
		return $options[$option];
	else
		return false;
}
?>
