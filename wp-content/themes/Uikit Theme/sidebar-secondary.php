<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-secondary')) : ?>
    <aside class="nst-sidebar">
        <?php dynamic_sidebar('sidebar-secondary'); ?>
    </aside>
<?php endif; ?>