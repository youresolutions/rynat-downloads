<?php
/**
 * element for header
 */
if (mytheme_option( 'header' )) {
     $header_style = '';
    /*if ($headerImageUrl = get_header_image()) {
        $header_style = 'style="height: 180px; background-image: url(' . $headerImageUrl . ');"';
    }*/
if (get_header_image()=='') {
     $header_style = 'style="height: ' . mytheme_option('headerheight') . 'px; background-color: #' . mytheme_option( 'headercolour' ) . ';"';
    }
    else {
        $header_style = 'style="height: ' . mytheme_option('headerheight') . 'px; background-image: url(' . get_header_image() . ');"';

    }
    $text_style = '';
 if (get_header_textcolor()=='blank') {
     $text_style='style="visibility: hidden;"'; }
    else {
        $text_style='style="color:#' . mytheme_option( 'headertextcolour' ) . ';"';

    }

?>

<header id="header" class="uk-cover-background uk-block uk-block-secondary uk-contrast uk-hidden-small" <?= $header_style ?>>
   <?php if (mytheme_option( 'fullscreen' ))
   { ?> <div class="uk-margin-large-left uk-margin-large-right"> <?php }
   else { ?>
    <div class="uk-container uk-container-center">
    <?php } ?>
    <div class="uk-grid">
    <?php if (mytheme_option( 'headerlogo' ) != '') : ?>
        <div class="uk-width-2-5">
            <img src="<?php echo mytheme_option( 'headerlogo' ) ?>" style="width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . "px"; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . "px"; } ?>">
        </div>
    <?php endif; ?>
    <div class="uk-width-3-5">
        <h1 <?= $text_style ?>>
            <a href="<?= esc_url(home_url('/')); ?>" title="<?= esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                <?php bloginfo('name'); ?>
            </a>
        </h1>
        <?php if ($description = get_bloginfo('description')) : ?>
            <span <?= $text_style ?>><?= $description ?></span>
        <?php endif; ?>
    </div>
    </div>
    </div>
</header>
<?php } ?>