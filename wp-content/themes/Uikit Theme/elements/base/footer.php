<?php
/**
 * element for footer
 */

$nav_footer = wp_nav_menu(array(
    'theme_location' => 'footer',
    'menu_class'     => 'uk-subnav uk-subnav-line',
    'depth'          => 1,
    'walker'         => new WordpressUikitMenuWalker('inline'),
    'echo'           => false,
    'fallback_cb'    => false
));

?>
<footer id="footer" class="uk-margin-top">
    <?php if (!mytheme_option('fullscreen')) { ?>
    <div class="uk-container uk-container-center">
        <?php } else { ?>
        <div class="uk-margin-large-left uk-margin-large-right">
        <?php } ?>
        <?php get_sidebar('footer'); ?>
        <?php if ($nav_footer) : ?>
            <nav class="uk-text-center">
                <?= $nav_footer ?>
            </nav>
        <?php endif; ?>

        <div class="uk-margin-top uk-text-center uk-text-muted">
            <div class="uk-panel">
            </div>
        </div>
    </div>
</footer>

