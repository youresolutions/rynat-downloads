<?php
/**
 * element pre content
 * should be rendered directly before the dynamic content
 * handles displaying of the sidebar (or no sidebar)
 * must be closed via the element postcontent
 */
 if (mytheme_option('twosidebars')) {
?>
<section id="main" class="uk-margin-large-left uk-margin-large-right uk-margin-large-top">
        <div class="uk-grid" data-uk-grid-margin  >
             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-medium-1-6">
                           <?php get_sidebar('sidebar-main'); ?>
                    </div>
                    <div class="uk-width-medium-4-6">
                        <?php else : ?>
                    <div class="uk-width-1-1">
                <?php endif; ?>
            <section id="content">

<?php } else { ?>

<section id="main" class="uk-margin-large-left uk-margin-large-right uk-margin-large-top">
        <div class="uk-grid" data-uk-grid-margin  >
            <?php if (mytheme_option('sidebarloc') == 'left') { ?>
            <?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-medium-1-4">
                           <?php get_sidebar(); ?>
                    </div>
            <?php endif; ?>
            <?php endif; ?>
            <?php } ?>
            <?php wp_reset_query(); ?>
            <?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
                <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-medium-3-4">
                        <?php else : ?>
                    <div class="uk-width-1-1">
                <?php endif; ?>
                <?php else : ?>
                    <div class="uk-width-1-1">
            <?php endif; ?>
            <section id="content">

<?php } ?>
