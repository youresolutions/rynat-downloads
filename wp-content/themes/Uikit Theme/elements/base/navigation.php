<?php
/**
 * element for navigation and offcanvas navigation
 */

$nav = wp_nav_menu(array(
    'theme_location' => 'main',
    'menu_class'     => 'uk-navbar-nav uk-hidden-small',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('navbar'),
    'echo'           => false,
    'fallback_cb'    => false
));

$nav_offcanvas = wp_nav_menu(array(
    'theme_location' => 'main',
    'menu_class'     => 'uk-nav uk-nav-offcanvas',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('navbar'),
    'echo'           => false,
    'fallback_cb'    => false
));

?>
<?php
    if ($nav) :
    if ((mytheme_option( 'fullscreen' )) && (mytheme_option( 'header' ))) {?>
            <div style="padding-top: 25px; padding-right: 25px; padding-left: 25px;">
            <?php  }
        elseif((!mytheme_option( 'header' )) && (mytheme_option( 'fullscreen' )) ) { ?>
            <div style="padding-top: 25px; padding-right: 25px; padding-left: 25px;">
            <?php }
        elseif((mytheme_option( 'header' )) && (!mytheme_option( 'fullscreen' )) ) { ?>
            <div class="uk-container uk-container-center">
        <?php } else { ?>
            <div class="uk-container uk-container-center" style="padding-top: 25px;">
        <?php } ?>
   <nav class="uk-navbar">
    <a href="#offcanvas-menu" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
    <?php if (trim(mytheme_option( 'headerlogo' )) != '') { ?> <a href="" class="uk-navbar-brand uk-visible-small uk-navbar-center"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" style="width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . "px"; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . "px"; } ?>"></a> <?php } ?>
    <?php if (!mytheme_option( 'header' )) { ?>
    <?php if (trim(mytheme_option( 'headerlogo' )) != '') { ?> <a href="" class="uk-navbar-brand uk-hidden-small"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" style="width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . "px"; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . "px"; } ?>"></a>
    <ul class="uk-navbar-nav uk-navbar-center uk-hidden-small"> <?php } else { ?>
    <ul class="uk-navbar-nav uk-hidden-small"> <?php } ?>
    <?php } else { ?>
    <ul class="uk-navbar-nav uk-hidden-small">
    <?php } ?>
            <?= $nav ?>
     </ul>

    <div class="uk-navbar-flip">
        <div class="uk-navbar-content uk-hidden-small">
                    <form class="uk-search" method="get" id="search_form" action="<?php bloginfo('home'); ?>"/>
                       <input type="text" class="uk-search-field" name="s" value="" Placeholder="Search" >
                    </form>
        </div>
    </div>
    </nav>
    <div id="offcanvas-menu" class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
            <?= $nav_offcanvas ?>
            <div class="uk-navbar-content uk-visable-small">
                    <form class="uk-search" method="get" id="search_form" action="<?php bloginfo('home'); ?>"/>
                       <input type="text" class="uk-search-field" name="s" value="" Placeholder="Search" >
                    </form>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (is_front_page()) {
    if (mytheme_option('slider')) {
        get_template_part('elements/content/ThemeSlideshow');
} }
?>
