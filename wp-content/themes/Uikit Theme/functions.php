<?php

require('php/Theme.php');

/**
 * Theme setup function
 */
/*Add any custom styles or js to this function*/
function wpb_adding_scripts()
{

}

add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );

/*load uikit theme*/
if (!function_exists('wp_uikit_starter_setup')) {
    function wp_uikit_starter_setup()
    {
        global $theme;
        $theme = new Theme();
    }

    add_action('after_setup_theme', 'wp_uikit_starter_setup');
}


/*load theme options*/
if ( file_exists( STYLESHEETPATH . '/class.my-theme-options.php' ) ) {
	require_once( STYLESHEETPATH . '/class.my-theme-options.php' );
}

/*Load in custom css from the theme options*/
add_action('wp_head','hook_css');

function hook_css() {

	echo '<style>';
	echo mytheme_option( 'custom_css' );
	echo '</style>';

}

/*Grab custom max width from theme options*/

add_action('wp_head','hook_width');

function hook_width() {

	echo '<style> .uk-custom-width { max-width: ';
	echo mytheme_option( 'Custom_Width' );
	echo 'px; margin: auto; background-color: #';
	echo mytheme_option( 'custombgcol') . '!important;}</style>';

}

add_action('wp_head','hook_fsbg');

function hook_fsbg() {

	echo '<style> .uk-custom-bg { background: #';
	echo mytheme_option( 'custombgcol' );
	echo ' !important;}</style>';

}



